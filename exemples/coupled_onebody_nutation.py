"""Demonstrates the nutation of a magnetic moment
precessing around the z axis.
see fig 2c of [Phys. Rev. B 99, 134409 (2019)].
"""
import numpy as np
from matplotlib import pyplot as plt
import tinyarray as ta
import kwant
import tkwant
import quantum_dynamics.coupled_onebody as coupled_onebody
from textures import General

s_0 = np.array([[1, 0], [0, 1]])
s_x = np.array([[0, 1], [1, 0]])
s_y = np.array([[0, -1j], [1j, 0]])
s_z = np.array([[1, 0], [0, -1]])


lat = kwant.lattice.square(norbs=2)
sym = kwant.TranslationalSymmetry((-1, 0))
lead = kwant.Builder(sym, conservation_law=s_x)

# magkwant system


def make_mag(L=3):
    # we define magnetic moments oriented along x
    mag = kwant.Builder()
    for i in range(L):
        mag[lat(i, 0)] = ta.array([1, 0, 0])
    mag[lat.neighbors()] = 1
    return mag

# pure kwant system


def make_system(L=3):
    syst = make_mag(L=L)
    for i in range(L):
        syst[lat(i, 0)] = 0 * s_0
    syst[lat.neighbors()] = -s_0
    lead[lat(0, 0)] = 0 * s_0
    lead[lat.neighbors()] = -s_0
    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())
    return syst


def main():
    # parameters
    B = 1000  # magnetic field strength
    Jsd = 40  # s-d coupling
    energy = 0
    magnetic_field = ta.array([0, 0, B])
    llg_constants = {"damping": .02, }

    syst = make_system()
    fsyst = syst.finalized()

    # pure `magkwant.textures` instance
    mag_syst = make_mag()
    magnet = General(mag_syst, llg_constants=llg_constants,
                     magnetic_field=magnetic_field)

    # coupled calculations
    scattering_states = kwant.wave_function(
        fsyst, energy=0, params={'time': 0})
    psi_st = scattering_states(0)[0]

    tmax = .125
    times = np.linspace(0, tmax, 200)
    boundaries = [tkwant.leads.SimpleBoundary(tmax=tmax)] * len(syst.leads)

    psi = coupled_onebody.WaveFunction.from_kwant(
        fsyst, psi_st, magnet, Jsd=Jsd, boundaries=boundaries, energy=energy)

    # evolve the texture in time
    current = []
    for time in times:
        psi.evolve(time)
        # m_z component of the central site
        current.append(psi.texture()[1][2])
    plt.xlabel("time")
    plt.ylabel(r"$m_z$")
    plt.plot(current)
    plt.show()


if __name__ == '__main__':
    main()
