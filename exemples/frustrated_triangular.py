import numpy as np
import tinyarray as ta
import matplotlib.pyplot as plt
from numpy import arccos, pi, sqrt
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import kwant
import magkwant.textures as textures
from helper import periodic


def make_system(J1=-1, J2=-.5, L=42, W=42):
    """
    returns a triangular antiferromagnet with periodic boundary conditions.
    """
    np.random.seed(41)
    lat = kwant.lattice.triangular()
    sys = kwant.Builder()
    for i in range(L):
        for j in range(W):
            # magnetization as onsite values
            sys[lat(i, j)] = np.random.uniform(low=-1, high=1, size=3)
    # Heisenberg exchange parameters as hoppings
    sys[lat.neighbors()] = J1
    sys[lat.neighbors(2)] = J2
    return periodic(sys)

# plotting function


def plot(fsyst, data, L=42, W=42):
    """
    plots data on fsyst and prints angles between spins
    at the midle of the system to check the texture.
    l and w define the site for which angles are computed.
    """
    mx = data[:, 0]
    my = data[:, 1]
    mz = data[:, 2]

    sites = (([site for site in (s for s in fsyst.sites)]))

    xpos = []
    ypos = []
    for k, site in enumerate(sites):
        x0, y0 = site.pos
        xpos.append(x0)
        ypos.append(y0)

    # pick the four sites of a unit cell and compute angles
    l = 10
    w = 10
    i, j = l//2, w//2
    a = 5 * l + l//2
    c = a + 1
    b = a + L
    d = b + 1

    for i in [b, c, d]:
        scalar = (np.dot(data[a], data[i]))
        print('angle in degree : ', 180 * arccos(scalar) / pi)

    fig, ax = plt.subplots(figsize=(14, 7))
    ax.scatter(xpos, ypos, color='black', s=7)
    cmap = plt.cm.seismic
    vects = ax.quiver(xpos, ypos, mx, my, mz, cmap=cmap)
    cbar = fig.colorbar(vects)
    cbar.set_label(label=r'$S_z$', fontsize=12, rotation=0)
    ax.axis('off')
    plt.show()

# magkwant computation


def texture(J2=-.5, B2=-.1, L=42, W=42):
    llg_constants = dict(damping=.05)
    magnet = make_system(J2=J2, L=L, W=W)
    field_inputs = dict(B2=B2)
    magnetization = textures.Triangular(magnet, field_inputs=field_inputs,
                                        llg_constants=llg_constants, pbc=True)
    magnet = magnet.finalized()
    return magnet, magnetization


def main():
    magnet, magnetization = texture()
    # evolving magnetization in time
    for ti in np.arange(0, 600, 100):
        plot(magnet, magnetization(ti))


if __name__ == "__main__":
    main()
