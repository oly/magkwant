"""
Solve the LLG equation for an inplane topological
ferromagnetic texture (Bimeron)
"""
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import kwant
import magkwant.textures as textures
from helper import periodic
import tinyarray as ta


def make_system(J1=.5, L=30, W=30):
    lat = kwant.lattice.square()
    sys = kwant.Builder()
    for i in range(-L, L+1):
        for j in range(-W, W+1):
            # we flipp moments at the center where
            # to stabilize the texture
            if i**2 + j**2 <= 10**2:
                sys[lat(i, j)] = - ta.array([1, 0, 0])
            else:
                sys[lat(i, j)] = ta.array([1, 0, 0])
    sys[lat.neighbors()] = J1
    return sys


def plot(fsyst, data):
    """
    plots data on fsyst and prints.
    """
    mx = data[:, 0]
    my = data[:, 1]
    mz = data[:, 2]

    sites = (([site for site in (s for s in fsyst.sites)]))

    xpos = []
    ypos = []
    for k, site in enumerate(sites):
        x0, y0 = site.pos
        xpos.append(x0)
        ypos.append(y0)

    fig, ax = plt.subplots(figsize=(7, 6))
    ax.scatter(xpos, ypos, color='black', s=7)
    cmap = plt.cm.seismic
    vects = ax.quiver(xpos, ypos, mx, my, mz, angles='xy',
                      scale_units='xy', scale=.7, cmap=cmap)
    cbar = fig.colorbar(vects)
    cbar.set_label(label=r'$S_x$', fontsize=12, rotation=0)
    plt.xlim(-20, 20)
    plt.ylim(-20, 20)
    ax.axis('off')
    plt.show()


def _bimeron_dij(sitei, sitej):
    """we construct a particular DMI for stabilizing a bimeron
    the DMI vectors are illustreted in Fig.1(c) of ref [1].
    """
    dmi_const = .5/3
    posi = sitei.pos
    posj = sitej.pos
    vd = np.array([0, 0, 1])
    uij = posj - posi
    normij = np.sqrt(sum(uij * uij))
    if normij > 1:
        uij *= 0  # to not include pbc in the DMI
    uij = np.divide(uij, normij, where=(normij != 0))
    dij = np.cross(vd, uij)
    if (posj - posi)[1] == 1:
        dij = vd
    elif (posj - posi)[1] == - 1:
        dij = - vd
    return dmi_const * dij


def texture(dmi=_bimeron_dij, L=30, W=30):
    llg_constants = dict(damping=.05)
    aniso = .5*(1/15)*ta.array([1, 0, 0])
    B = (1/15) * ta.array([1, 0, 0])
    inputs = dict(dmi=dmi, aniso=aniso)
    sys = make_system(L=L, W=W)
    sys = periodic(sys)
    m = textures.General(sys, field_inputs=inputs,
                         llg_constants=llg_constants, magnetic_field=B)
    return sys.finalized(), m


def main():
    sys, m = texture(L=50, W=50)
    for ti in np.arange(390, 480, 10):
        plot(sys, m(ti))


if __name__ == "__main__":
    main()

#############################################################
# reference for the parameters:
# [1] Phys. Rev. B 99, 060407(R) – Published 25 February 2019
