"""
Solve the LLG equation for a  ferromagnetic
skyrmion with and without dipole interaction
"""
import matplotlib.pyplot as plt
import numpy as np
from numpy import arccos, pi, sqrt
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import kwant
import magkwant.textures as textures
from helper import periodic
import tinyarray as ta


def make_system(J1=.5, L=30, W=30):
    lat = kwant.lattice.square()
    sys = kwant.Builder()
    for i in range(-L, L+1):
        for j in range(-W, W+1):
            if i**2 + j**2 <= 10**2:
                # we flipp moments at the center where
                # to stabilize the texture
                sys[lat(i, j)] = - ta.array([0, 0, 1])
            else:
                sys[lat(i, j)] = ta.array([0, 0, 1])
    sys[lat.neighbors()] = J1
    return sys


def plot(fsyst, data):
    """
    plots data on fsyst.
    """
    mx = data[:, 0]
    my = data[:, 1]
    mz = data[:, 2]

    sites = (([site for site in (s for s in fsyst.sites)]))

    xpos = []
    ypos = []
    for k, site in enumerate(sites):
        x0, y0 = site.pos
        xpos.append(x0)
        ypos.append(y0)

    fig, ax = plt.subplots(figsize=(7, 6))
    ax.scatter(xpos, ypos, color='black', s=7)
    cmap = plt.cm.seismic
    vects = ax.quiver(xpos, ypos, mx, my, mz, angles='xy',
                      scale_units='xy', scale=.7, cmap=cmap)
    cbar = fig.colorbar(vects)
    cbar.set_label(label=r'$S_z$', fontsize=12, rotation=0)
    plt.xlim(-20, 20)
    plt.ylim(-20, 20)
    ax.axis('off')
    plt.show()


def texture(dmi=.5/3, L=50, W=50):
    llg_constants = dict(damping=.05)
    aniso = .5 * (1/15) * ta.array([0, 0, 1])
    B = (1/15) * ta.array([0, 0, 1])
    inputs = dict(dmi=dmi, aniso=aniso)
    sys = make_system(L=L, W=W)
    sys = periodic(sys)
    m = textures.General(sys, field_inputs=inputs,
                         llg_constants=llg_constants, magnetic_field=B)
    return sys.finalized(), m


def main():
    sys, m = texture(L=50, W=50)
    for time in np.arange(330, 350, 5):
        plot(sys, m(time))


if __name__ == "__main__":
    main()

#############################################################
# reference for the parameters:
# [1] Phys. Rev. B 99, 060407(R) – Published 25 February 2019
