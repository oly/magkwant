from setuptools import setup, Extension
from Cython.Build import cythonize

interactions = ['common', 'supplemental']


def _extension(interaction):
    return Extension('magkwant.fields.'+interaction+'_fields',
                     sources=['magkwant/fields/'+interaction+'_fields.pyx'])


exts = [_extension('common'),  _extension('supplemental'), Extension('magkwant.fields.m_cross_j_scalar_nabla',
                                                                     sources=['magkwant/fields/m_cross_j_scalar_nabla.pyx'])]

requirements = (
    "cython>=0.29.13",
    "numpy>=1.8.2",
    "scipy>=0.14.0",
    "kwant>=1.3",
    "tinyarray",
    "tkwant",
)


def main():
    setup(
        name='magkwant',
        version='1.0.0',
        url='https://gitlab.kwant-project.org/oly/magkwant',
        author='magkwant authors',
        author_email='ousmane.ly@kaust.edu.sa',
        description='Package for selfconsistently solving \
         classical magnetization dynamics with time dependent quantum transport.',
        ext_modules=cythonize(exts)
    )


if __name__ == "__main__":
    main()
