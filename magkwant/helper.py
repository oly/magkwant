from itertools import islice
import numpy as np
import tinyarray as ta

__all__ = ["periodic"]


class Learn_from_central_site:
    """
    Learns from the maximally surronded site and get all the possible moves and
    hopping values.
    Parmeters:
    ----------
    sys: kwant.Builder
        The sysem to which periodic boundary conditions are applied.
    Attributes:
    ----------
    z: int
        Maximum coordination number.
    moves: dict
        A dictionary stocking all the possible moves from a maximally surounded site,
        together with the underlying hoppings.
    min_tag: tinyarray.array
        Minimal tag within sys.
    max_tag: tinyarray.array
        Maximal tag within sys.
    period: tinyarray.array
        The period of the boundary conditions in all directions.
    """

    def __init__(self, sys):
        self.sys = sys
        central = []
        self.legitimate_tags = {}
        for tail, hvhv in self.sys.H.items():
            self.legitimate_tags[tail.tag] = tail
            central.append((self.sys._out_degree(tail), hvhv, tail))
        self.z, central_hvhv, self.central_site = max(central)
        self.central_tag = self.central_site.tag

        self.min_tag = min(self.legitimate_tags)
        self.max_tag = max(self.legitimate_tags)
        self.period = self.max_tag - self.min_tag + 1

        # we couple the surrounding heads to their values
        h_v = zip(islice(central_hvhv, 2, None, 2),
                  islice(central_hvhv, 3, None, 2))
        # get all possible moves from the maximally surrounded central site
        self.moves = {head.tag -
                      self.central_tag: value for head, value in h_v}


class Make_periodic(Learn_from_central_site):
    """
    A class to make periodic finite Builders.
    It should be used only for systems well described by kwant tags.
    i.e. complete unit cells are assumed.
    #TODO: make it more general.
    Parameters:
    ----------
    sys: kwant.Builder
    """

    def __init__(self, sys):
        super().__init__(sys=sys)
        self.periferic_sites = []
        for tail, _ in self.sys.H.items():
            if self.sys._out_degree(tail) < self.z:
                self.periferic_sites.append(tail)

    def _jump_back(self, move, arrival):
        """
        Returns the tag of the head that should replace `arrival` within the existing sites.
        With arrival being a non valid tag after a legitimate move learnt from the central site.
        """
        new_head = np.array(arrival - np.sign(move) * self.period)
        for di in range(move.shape[0]):
            if self.min_tag[di] <= arrival[di] <= self.max_tag[di]:
                # if one direction of the arrival site reside within
                # the underlyin system's dimension then it's coordinate
                # along such dimension is kept intact
                new_head[di] = arrival[di]
        return ta.array(new_head)

    def apply_pbc_to(self, site):
        for move, value in self.moves.items():
            arrival = site.tag + move
            if arrival not in self.legitimate_tags:
                arrival = self._jump_back(move, arrival)
                if arrival in self.legitimate_tags:
                    final_head = self.legitimate_tags[arrival]
                    self.sys._set_edge(site, final_head, value)

    def __call__(self):
        for tail in self.periferic_sites:
            self.apply_pbc_to(tail)
        return self.sys


def periodic(sys):
    return Make_periodic(sys)()
