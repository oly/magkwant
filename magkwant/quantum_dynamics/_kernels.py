import numpy as np
from numpy import exp
import scipy
import tinyarray as ta

__all__ = ['CoupledScipy']

pauli_x = ta.array([[0, 1], [1, 0]])
pauli_y = ta.array([[0, -1j], [1j, 0]])
pauli_z = ta.array([[1, 0], [0, -1]])


def normalized(array):
    norm = scipy.linalg.norm(array, axis=1, keepdims=True)
    return np.divide(array, norm, where=(norm != 0))

# Concrete kernel implementations of the coupled llg-tkwant kernel


class CoupledScipy:
    # TODO : move this to Cython
    """Evaluate the RHS of the Schrödinger equation using scipy sparse.

    Parameters
    ----------
    H0 : `scipy.sparse.base`
        Hamiltonian at ``t=0`` (including any boundary conditions).
    W : callable
        Time-dependent part of the Hamiltonian. Typically the object returned
        by `tkwant.system.extract_perturbation`.
    params : dict
        Extra arguments to pass to the system Hamiltonian excluding time.
    psi_st : array of complex, optional
        The wavefunction of the initial eigenstate defined over the center
        system (if starting in an initial eigenstate).
    F : calable
        The calculator of the effective felt by the magnetic moments.
        Typically the object returned by `magkwant.textures.General.effective_field`.
    energy : float, optional
            If provided, then ``psi_st`` is assumed to be an eigenstate
            of energy *E*. If ``syst`` has leads, then ``psi_st`` is
            assumed to be the projection of a scattering state at energy *E*
            on to the central part of the system.
    time_start : float, optional
        The initial time :math:`t_0`. Default value is zero.
    damping : float
        The damping rate in the LLG equation.
    spin_basis : dict, optional
        keys : directions in spin space 'x', 'y' and 'z'.
        values : the corresponding spin matrices to be used to
        compute the sd interaction between the magnetic moments
        and the itinerant electrons. The matrices should square matrices
        of the same shape as the energy terms used in defining the
        quantum system.
        The default matrices correspond to the 2x2 Pauli matrices.
    Jsd : float
        The strength of the sd interaction between the magnetic moments
        and the itinerant electrons.

    Attributes
    ----------
    H0 : `scipy.sparse.csr_matrix`
        Hamiltonian at ``t=0`` (including any boundary conditions).
    W : callable
        Time-dependent part of the Hamiltonian. Typically the object returned
        by `tkwant.system.extract_perturbation`.
    params : dict
        Extra arguments to pass to the system Hamiltonian excluding the ``time``
        argument.
    psi_st : array of complex or `None`
        The wavefunction of the initial eigenstate defined over the center
        system (if starting in an initial eigenstate).
    size : int
        The size of the time-independent part of the Hamiltonian. This
        also sets the size of the solution vector.
    nevals : int
        The number of times this kernel has been evaluated since its creation.
    """

    def __init__(self, H0, W, F, params, psi_st=None, damping=.1, gamma=1, mus=1,
                 time_start=0, energy=None, Jsd=0, spin_basis=None):
        self.H0 = H0.tocsr()
        self.W = W
        self.F = F
        self.psi_st = psi_st
        self.size = H0.shape[0]

        self.params = params
        self.damping = damping
        self.Jsd = Jsd
        self.time_start = time_start
        self.energy = energy

        if spin_basis is None:
            self.spin_x = pauli_x
            self.spin_y = pauli_y
            self.spin_z = pauli_z
        else:
            self.spin_x = spin_basis["x"]
            self.spin_y = spin_basis["y"]
            self.spin_z = spin_basis["z"]

        self.norbs = self.spin_x.shape[0]
        self.coupling_size = H0.shape[0] + 3 * self.W.size//self.norbs
        self.nevals = 0

        self.hbar = 1
        self.gamma = gamma
        self.mus = mus

    def set_params(self, params):
        self.params = params

    def rhs(self, psi_m, dpsi_mdt, time, m=None):
        """Evaluate the RHS of the TDSE and store the result in `dpsi_mdt`.
        Parameters
        ----------
        psi_m : ndarray
           the vector containing the wave functions and the magnetization
           vectors. It is ordered as follows
           :math: `\psi_m = [\psi, m_x, m_y, m_z].T`
        dpsi_mdt : ndarray
           the evaluated rhs of the coupled dynamic equation.
        time : float
           the moment at which the kernel is evaluated.
        source : ndarray or None
           local spin density acting as a torque on the magnetic
           vector.

        Note:
        -----
        In the case of manybody calculations `source` should be
        parallely computed and passed to the kernel for each single
        onebody state.

        In a pure onebody calculation `source` is not given.
        It's rather computed as the expectation value of the spin
        matrices vector.
        """

        lamda = self.damping
        prefector = - self.gamma / (1 + lamda**2)
        center = self.W.size

        m_size = center//self.norbs
        psi = psi_m[:self.size]

        if self.energy is not None:
            psi_center = ((psi[:center] + self.psi_st) *
                          exp(-1j * self.energy * (time - self.time_start)))
        else:
            psi_center = psi[:center]

        _psi_center = psi_center.reshape(center//self.norbs, self.norbs)

        # H0 @ psi -> tmp
        tmp = self.H0.dot(psi)

        # W(t) @ (psi + psi_st) + tmp -> tmp
        try:
            self.W.apply(time, psi_center[:center], out=tmp[:center])

        except Exception as exept:
            if self.W is None:
                pass
            else:
                raise exept

        # since the interaction between m and psi is assumed only in the center region,
        # we reshape the central part of tmp to be vertically of the same size of m
        # and make a new version of it
        _tmp = tmp[:center].reshape((m_size, self.norbs))

        # we similarly reshape the center part of dpsi_mdt to be vertically of the same size of m
        # and make a new version of it
        _dpsi_mdt = dpsi_mdt[:center].reshape((m_size, self.norbs))

        if m is None:
            m = np.reshape(psi_m[self.size:], (3, m_size)).T
            # compute the llg effective field
            h_effective = self.F(time, m)
            s_x = np.diag(_psi_center.conjugate() @
                          self.spin_x @ (_psi_center.T))
            s_y = np.diag(_psi_center.conjugate() @
                          self.spin_y @ (_psi_center.T))
            s_z = np.diag(_psi_center.conjugate() @
                          self.spin_z @ (_psi_center.T))
            source = np.array((s_x, s_y, s_z)).T

            h_effective += self.Jsd * normalized(source.real) / self.mus

            # we compute the llg rhs modified by the quantum torque
            tmp_m = prefector * (np.cross(m, h_effective) +
                                 lamda * np.cross(m, np.cross(m, h_effective)))
            # then the remaining part of dpsi_dmt should be filled with the three components
            # of the magnetization vector
            dpsi_mdt[self.size:] = tmp_m.flatten("f")

        # we compute the quantum part of the resized version of dpsi_mdt modified by the sd interaction m \cdot \sigma
        _dpsi_mdt[:center] = -1j * (_tmp - self.Jsd * (m[:, 0] * (self.spin_x @ _psi_center.T)).T
                                         - self.Jsd *
                                    (m[:, 1] * (self.spin_y @ _psi_center.T)).T
                                    - self.Jsd * (m[:, 2] * (self.spin_z @ _psi_center.T)).T) / self.hbar

        # after computing the mutual interactions, we get back to the normal size of the coupled rhs vector
        # we first fill the central part with wavefunctions
        dpsi_mdt[:center] = _dpsi_mdt.reshape(center)

        # we fill the boundary part of the dpsi_mdt which is not affected by m
        dpsi_mdt[center:self.size] = -1j * tmp[center:self.size] / self.hbar
        self.nevals += 1
