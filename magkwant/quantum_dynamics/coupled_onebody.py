"""Calculations of LLG-tkwant manybody states.
The underlying classes inherit from tkwant.manybody classes.
Some doc strings from tkwant are used.
"""
from quantum_dynamics import _solvers, _kernels
import functools
import numpy as np
import scipy.sparse as sp
import kwant
import tkwant
from tkwant.system import (hamiltonian_with_boundaries,
                           add_time_to_params)
from tkwant import _common, _logging, leads
import kwantspectrum


__all__ = ['WaveFunction', 'ScatteringStates']

# set module logger
logger = _logging.make_logger(name=__name__)
log_func = _logging.log_func(logger)


class WaveFunction(tkwant.onebody.onebody.WaveFunction):
    def __init__(self, H0, W, psi_init, m_init, F=None, energy=None, params=None,
                 solution_is_valid=None, time_is_valid=None, time_start=0, time_name='time',
                 kernel_type=_kernels.CoupledScipy, solver_type=_solvers.Coupled,
                 time_step=None, damping=0.1, Jsd=1, gamma=1, mus=1, spin_basis=None):
        super().__init__(H0=H0, W=W, psi_init=psi_init, energy=energy, params=params,
                         solution_is_valid=solution_is_valid, time_is_valid=time_is_valid,
                         time_start=time_start, time_name=time_name,
                         kernel_type=kernel_type, solver_type=solver_type)
        # The size of the central scattering region. The central scattering
        # region can be smaller as the total size (kernel.size) of the system
        # in case of boundary conditions.
        syst_size = psi_init.size
        hamiltonian_size = H0.shape[0]

        if syst_size > hamiltonian_size:
            raise ValueError('initial condition size={} is larger than the '
                             'Hamiltonian matrix H0 size={}'
                             .format(syst_size, hamiltonian_size))

        # The perturbation W(t) must, if present, always match the size of the
        # central scattering region. The true leads are never time dependent.
        if W is not None:
            if not W.size == syst_size:
                raise ValueError('initial condition size={} must be equal '
                                 'to the perturbation W size={}'
                                 .format(syst_size, W.size))
        else:
            def W(*args, **kwargs):
                pass
            W.size = syst_size
        # add initial time to the params dict
        self.tparams = add_time_to_params(params, time_name=time_name,
                                          time=time_start, check_numeric_type=True)
        if energy is None:
            # starting from an arbitrary state, so we need
            # to be solving: H0 @ psi + W(t) @ psi
            self.kernel = kernel_type(H0, W, F, params=self.tparams,
                                      damping=damping, time_start=time_start,
                                      energy=energy, Jsd=Jsd, gamma=gamma, mus=mus, spin_basis=spin_basis)

        else:
            # we are starting from an eigenstate, so we need to
            # be solving: (H0 - E) @ psibar + W(t) @ (psibar + psi_st)
            # and psi = (psibar + psi_st) * exp(-1j * energy * time)
            self.kernel = kernel_type(H0 - energy * sp.eye(hamiltonian_size),
                                      W, F, self.tparams, np.asarray(
                                          psi_init), damping, gamma, mus,
                                      time_start, energy, Jsd, spin_basis)

        # get the object that will actually do the time stepping
        self.solver = solver_type(self.kernel, time_step)
        syst_size = psi_init.size

        psibar = np.zeros((self.kernel.coupling_size,), complex)

        if energy is not None:
            psi_st = np.array(psi_init, complex)
        else:
            psi_st = None
            psibar[:syst_size] = psi_init

        psibar[self.kernel.size:] = m_init.flatten("f")
        self.psibar = psibar
        self.psi_st = psi_st

        self._syst_size = syst_size
        self.m_size = m_init.shape[0]
        self._solution_is_valid = solution_is_valid
        self._time_is_valid = time_is_valid

        self.time_name = time_name
        self.time_start = time_start

        self.time = time_start
        self.params = params
        self.energy = energy
        self.kernel_size = self.kernel.size

    @classmethod
    def from_kwant(cls, syst, psi_init, magnet, Jsd=0, spin_basis=None,
                   boundaries=None, energy=None, params=None, time_start=0, time_name='time',
                   kernel_type=_kernels.CoupledScipy, solver_type=_solvers.Coupled,
                   perturbation_type=tkwant.onebody.kernels.PerturbationInterpolator):
        """Set up a time-dependent onebody wavefunction from a kwant system.

        Parameters
        ----------
        syst : `kwant.builder.FiniteSystem`
            The low level system for which the wave functions are to be
            calculated.
        psi_init : array of complex
            The state from which to start, defined over the central region.
        boundaries : sequence of `~tkwant.leads.BoundaryBase`, optional
            The boundary conditions for each lead attached to ``syst``.
            Must be provided for a system with leads.
        energy : float, optional
            If provided, then ``psi_init`` is assumed to be an eigenstate
            of energy *E*. If ``syst`` has leads, then ``psi_init`` is
            assumed to be the projection of a scattering state at energy *E*
            on to the central part of the system.
        params : dict, optional
            Extra arguments to pass to the Hamiltonian of ``syst``, excluding time.
        time_start : float, optional
            The initial time :math:`t_0`. Default value is zero.
        time_name : str, optional
            The name of the time argument :math:`t`. Default name: *time*.
        kernel_type : `tkwant.onebody.solvers.default`, optional
            The kernel to calculate the right-hand-site of the
            Schrödinger equation.
        solver_type : `tkwant.onebody.solvers.default`, optional
            The solver used to evolve the wavefunction forward in time.
        magnet : `magkwant.textures.General`
            The calculator of the effective field underlying
            the magnetic system.
        Jsd : float
            The sd coupling between the classical magnetic vectors
            and the the electronic spins.
        spin_basis : dict
            The spin matrices used in add the sd interaction to the
            Hamiltonion. All spin matrices should have the same shape
            as the onsite potentials used to define the tkwant system.

        Returns
        -------
        wave_function : `tkwant.onebody.WaveFunction`
            A time-dependent onebody wavefunction at the initial time.
        """

        syst_size = syst.site_ranges[-1][2]
        if not psi_init.size == syst_size:
            raise ValueError('Size of the initial condition={} does not match '
                             'the total number of orbitals in the central '
                             'system ={}'.format(psi_init.size, syst_size))

        # add initial time to the params dict
        tparams = add_time_to_params(params, time_name=time_name,
                                     time=time_start, check_numeric_type=True)

        if syst.leads:
            # need to add boundary conditions
            if not boundaries:
                raise ValueError('Must provide boundary conditions for systems '
                                 'with leads.')
            # get static part of Hamiltonian for central system + boundary conditions
            extended_system = hamiltonian_with_boundaries(syst, boundaries,
                                                          params=tparams)
            H0 = extended_system.hamiltonian
            solution_is_valid = extended_system.solution_is_valid
            time_is_valid = extended_system.time_is_valid
        else:
            # true finite systems (no leads) so no need for boundary conditions
            H0 = syst.hamiltonian_submatrix(params=tparams, sparse=True)
            solution_is_valid = None
            time_is_valid = None

        W = perturbation_type(syst, time_name, time_start, params=params)
        # LLG related quantities
        m_init = magnet.m_initial
        damping = magnet.damping
        time_step = magnet.time_step
        F = magnet.effective_field
        gamma = magnet.gamma
        mus = magnet.mus
        return cls(H0, W, psi_init, m_init, F, energy, params,
                   solution_is_valid, time_is_valid,
                   time_start, time_name, _kernels.CoupledScipy,
                   solver_type, time_step, damping, Jsd, gamma, mus, spin_basis)

    def texture(self):
        return np.reshape(self.psibar[self.kernel_size:], (3, self.m_size)).T

    def evolve(self, time, source=None, params=None):
        r"""
        Evolve the wavefunction :math:`\psi(t)` foreward in time up to
        :math:`t =` ``time``.

        Parameters
        ----------
        time : int or float
            time argument up to which the solver should be evolved
        params : dict, optional
            Extra arguments to pass to the Hamiltonian, excluding time.
            If present, the public ``params`` attribute is
            updated to the new value of ``params``.
            By default, the ``params`` from initialization are taken.
        """
        if params is not None:
            self.params = params
            self.solver.kernel.set_params(params)

        # `time` corresponds to the future time, to which we will evolve
        if time < self.time:
            raise ValueError('Cannot evolve backwards in time')
        if time == self.time:
            return
        if self._time_is_valid is not None:
            if not self._time_is_valid(time):
                raise RuntimeError('Cannot evolve up to {} with the given '
                                   'boundary conditions'.format(time))

        # evolve forward in time
        next_psibar = self.solver(self.psibar, self.time, time, source)

        if self._solution_is_valid is not None:
            if not self._solution_is_valid(next_psibar):
                raise RuntimeError('Evolving between {} and {} resulted in an '
                                   'unphysical result due to the boundary '
                                   'conditions'.format(self.time, next_time=time))
        # update internal state and return
        self.psibar, self.time = next_psibar, time

    def evaluate_vector(self, observables):
        r"""
        Evaluate the expectation value of an operator at the current time *t*.

        For an operator :math:`\hat{O}` the expectation value is
        :math:`O(t) = \langle \psi(t) | \hat{O} |\psi(t) \rangle`.

        Parameters
        ----------
        observable : callable or `kwant.operator`
            An operator :math:`\hat{O}` to evaluate the expectation value.
            Must have the calling signature of `kwant.operator`.

        Returns
        -------
        result : numpy array
            The expectation value :math:`O(t)` of ``observable``.
        """
        # if _operator_bound(observable):
        #    raise ValueError("Operator must not use pre-bind values")

        tparams = add_time_to_params(self.params, time_name=self.time_name,
                                     time=self.time)
        return np.array(observables[0](self.psi(), params=tparams),
                        observables[1](self.psi(), params=tparams),
                        observables[2](self.psi(), params=tparams)).T


class ScatteringStates(tkwant.onebody.onebody.ScatteringStates):
    def __init__(self, syst, magnet, energy, lead, Jsd=0, spin_basis=None, tmax=None, params=None,
                 spectra=None, boundaries=None, equilibrium_solver=kwant.wave_function,
                 wavefunction_type=WaveFunction.from_kwant):
        super().__init__(syst=syst, energy=energy, lead=lead, tmax=tmax, params=params,
                         spectra=spectra, boundaries=boundaries, equilibrium_solver=equilibrium_solver,
                         wavefunction_type=wavefunction_type)

        if not isinstance(syst, kwant.system.System):
            raise TypeError('"syst" must be a finalized kwant system')
        if not syst.leads:
            raise AttributeError("system has no leads.")
        if tmax is not None and boundaries is not None:
            raise ValueError("'boundaries' and 'tmax' are mutually exclusive.")
        if not _common.is_type(energy, 'real_number'):
            raise TypeError('energy must be a real number')
        if not _common.is_type(lead, 'integer'):
            raise TypeError('lead index must be an integer')
        if lead >= len(syst.leads):
            raise ValueError(
                "lead index must be smaller than {}.".format(len(syst.leads)))

        # get initial time and time argument name from the onebody wavefunction
        try:
            time_name = _common.get_default_function_argument(wavefunction_type,
                                                              'time_name')
            time_start = _common.get_default_function_argument(wavefunction_type,
                                                               'time_start')
        except Exception:
            time_name = _common.time_name
            time_start = _common.time_start
            logger.warning('retrieving initial time and time argument name from',
                           'the onebody wavefunction failed, use default values: ',
                           '"time_name"={}, "time_start"={}'.format(time_name,
                                                                    time_start))

        # add initial time to the params dict
        tparams = add_time_to_params(params, time_name=time_name,
                                     time=time_start, check_numeric_type=True)

        if boundaries is None:
            if spectra is None:
                spectra = kwantspectrum.spectra(syst.leads, params=tparams)
            boundaries = leads.automatic_boundary(spectra, tmax)

        scattering_states = equilibrium_solver(
            syst, energy=energy, params=tparams)
        self._psi_st = scattering_states(lead)
        self._wavefunction = functools.partial(wavefunction_type, syst=syst, magnet=magnet,
                                               Jsd=Jsd, spin_basis=spin_basis, boundaries=boundaries, params=params)
        self.energy = energy
        self.lead = lead


class DeCoupledWaveFunction(tkwant.onebody.onebody.WaveFunction):
    def __init__(self, H0, W, psi_init, energy=None, params=None,
                 solution_is_valid=None, time_is_valid=None, time_start=0, time_name='time',
                 kernel_type=_kernels.DeCoupledScipy, solver_type=_solvers.DeCoupled, time_step=0.001):
        super().__init__(H0=H0, W=W, psi_init=psi_init, energy=energy, params=params,
                         solution_is_valid=solution_is_valid, time_is_valid=time_is_valid,
                         time_start=time_start, time_name=time_name,
                         kernel_type=kernel_type, solver_type=solver_type)
        # The size of the central scattering region. The central scattering
        # region can be smaller as the total size (kernel.size) of the system
        # in case of boundary conditions.
        syst_size = psi_init.size
        hamiltonian_size = H0.shape[0]

        if syst_size > hamiltonian_size:
            raise ValueError('initial condition size={} is larger than the '
                             'Hamiltonian matrix H0 size={}'
                             .format(syst_size, hamiltonian_size))

        # The perturbation W(t) must, if present, always match the size of the
        # central scattering region. The true leads are never time dependent.
        if W is not None:
            if not W.size == syst_size:
                raise ValueError('initial condition size={} must be equal '
                                 'to the perturbation W size={}'
                                 .format(syst_size, W.size))
        else:
            def W(*args, **kwargs):
                pass
            W.size = syst_size
        # add initial time to the params dict
        self.tparams = add_time_to_params(params, time_name=time_name,
                                          time=time_start, check_numeric_type=True)
        if energy is None:
            # starting from an arbitrary state, so we need
            # to be solving: H0 @ psi + W(t) @ psi
            self.kernel = kernel_type(H0, W, params=self.tparams)

        else:
            # we are starting from an eigenstate, so we need to
            # be solving: (H0 - E) @ psibar + W(t) @ (psibar + psi_st)
            # and psi = (psibar + psi_st) * exp(-1j * energy * time)
            self.kernel = kernel_type(H0 - energy * sp.eye(hamiltonian_size),
                                      W, self.tparams, np.asarray(
                                          psi_init))

        # get the object that will actually do the time stepping
        self.solver = solver_type(self.kernel, time_step)
        syst_size = psi_init.size

        psibar = np.zeros((self.kernel.size,), complex)

        if energy is not None:
            psi_st = np.array(psi_init, complex)
        else:
            psi_st = None
            psibar[:syst_size] = psi_init

        self.psibar = psibar
        self.psi_st = psi_st

        self._syst_size = syst_size
        self._solution_is_valid = solution_is_valid
        self._time_is_valid = time_is_valid

        self.time_name = time_name
        self.time_start = time_start

        self.time = time_start
        self.params = params
        self.energy = energy
        self.kernel_size = self.kernel.size

    @classmethod
    def from_kwant(cls, syst, psi_init, boundaries=None, energy=None, params=None,
                   time_start=0, time_name='time', time_step=0.001,
                   kernel_type=_kernels.DeCoupledScipy, solver_type=_solvers.DeCoupled,
                   perturbation_type=tkwant.onebody.kernels.PerturbationInterpolator):
        """Set up a time-dependent onebody wavefunction from a kwant system.

        Parameters
        ----------
        syst : `kwant.builder.FiniteSystem`
            The low level system for which the wave functions are to be
            calculated.
        psi_init : array of complex
            The state from which to start, defined over the central region.
        boundaries : sequence of `~tkwant.leads.BoundaryBase`, optional
            The boundary conditions (Coupled)for each lead attached to ``syst``.
            Must be provided for a s(Coupled)ystem with leads.
        energy : float, optional(Coupled)
            If provided, then ``psi_(Coupled)init`` is assumed to be an eigenstate
            of energy *E*. If ``syst`` has leads, then ``psi_init`` is
            assumed to be the projection of a scattering state at energy *E*
            on to the central part of the system.
        params : dict, optional
            Extra arguments to pass to the Hamiltonian of ``syst``, excluding time.
        time_start : float, optional
            The initial time :math:`t_0`. Default value is zero.
        time_name : str, optional
            The name of the time argument :math:`t`. Default name: *time*.
        kernel_type : `tkwant.onebody.solvers.default`, optional
            The kernel to calculate the right-hand-site of the
            Schrödinger equation.
        solver_type : `tkwant.onebody.solvers.default`, optional
            The solver used to evolve the wavefunction forward in time.
        magnet : `magkwant.textures.General`
            The calculator of the effective field underlying
            the magnetic system.
        Jsd : float
            The sd coupling between the classical magnetic vectors
            and the the electronic spins.
        spin_basis : dict
            The spin matrices used in add the sd interaction to the
            Hamiltonion. All spin matrices should have the same shape
            as the onsite potentials used to define the tkwant system.

        Returns
        -------
        wave_function : `tkwant.onebody.WaveFunction`
            A time-dependent onebody wavefunction at the initial time.
        """

        syst_size = syst.site_ranges[-1][2]
        if not psi_init.size == syst_size:
            raise ValueError('Size of the initial condition={} does not match '
                             'the total number of orbitals in the central '
                             'system ={}'.format(psi_init.size, syst_size))

        # add initial time to the params dict
        tparams = add_time_to_params(params, time_name=time_name,
                                     time=time_start, check_numeric_type=True)

        if syst.leads:
            # need to add boundary conditions
            if not boundaries:
                raise ValueError('Must provide boundary conditions for systems '
                                 'with leads.')
            # get static part of Hamiltonian for central system + boundary conditions
            extended_system = hamiltonian_with_boundaries(syst, boundaries,
                                                          params=tparams)
            H0 = extended_system.hamiltonian
            solution_is_valid = extended_system.solution_is_valid
            time_is_valid = extended_system.time_is_valid
        else:
            # true finite systems (no leads) so no need for boundary conditions
            H0 = syst.hamiltonian_submatrix(params=tparams, sparse=True)
            solution_is_valid = None
            time_is_valid = None

        W = perturbation_type(syst, time_name, time_start, params=params)

        return cls(H0, W, psi_init, energy, params,
                   solution_is_valid, time_is_valid,
                   time_start, time_name, _kernels.DeCoupledScipy,
                   solver_type, time_step)

    def evolve(self, time, m=None, params=None):
        r"""
        Evolve the wavefunction :math:`\psi(t)` foreward in time up to
        :math:`t =` ``time``.

        Parameters
        ----------
        time : int or float
            time argument up to which the solver should be evolved
        params : dict, optional
            Extra arguments to pass to the Hamiltonian, excluding time.
            If present, the public ``params`` attribute is
            updated to the new value of ``params``.
            By default, the ``params`` from initialization are taken.
        """
        if params is not None:
            self.params = params
            self.solver.kernel.set_params(params)

        # `time` corresponds to the future time, to which we will evolve
        if time < self.time:
            raise ValueError('Cannot evolve backwards in time')
        if time == self.time:
            return
        if self._time_is_valid is not None:
            if not self._time_is_valid(time):
                raise RuntimeError('Cannot evolve up to {} with the given '
                                   'boundary conditions'.format(time))

        # evolve forward in time
        next_psibar = self.solver(self.psibar, self.time, time, m)

        if self._solution_is_valid is not None:
            if not self._solution_is_valid(next_psibar):
                raise RuntimeError('Evolving between {} and {} resulted in an '
                                   'unphysical result due to the boundary '
                                   'conditions'.format(self.time, next_time=time))
        # update internal state and return
        self.psibar, self.time = next_psibar, time

    def evaluate_vector(self, d_x, d_y, d_z):
        r"""
        Evaluate the expectation value of an operator at the current time *t*.

        For an operator :math:`\hat{O}` the expectation value is
        :math:`O(t) = \langle \psi(t) | \hat{O} |\psi(t) \rangle`.

        Parameters
        ----------
        observable : callable or `kwant.operator`
            An operator :math:`\hat{O}` to evaluate the expectation value.
            Must have the calling signature of `kwant.operator`.

        Returns
        -------
        result : numpy array
            The expectation value :math:`O(t)` of ``observable``.
        """
        # if _operator_bound(observable):
        #    raise ValueError("Operator must not use pre-bind values")

        tparams = add_time_to_params(self.params, time_name=self.time_name,
                                     time=self.time)
        return np.array((d_x(self.psi(), params=tparams), d_y(self.psi(), params=tparams), d_z(self.psi(), params=tparams))).T


class DeCoupledScatteringStates(tkwant.onebody.onebody.ScatteringStates):
    def __init__(self, syst,  energy, lead, tmax=None, params=None,
                 spectra=None, boundaries=None, equilibrium_solver=kwant.wave_function,
                 wavefunction_type=DeCoupledWaveFunction.from_kwant):
        super().__init__(syst=syst, energy=energy, lead=lead, tmax=tmax, params=params,
                         spectra=spectra, boundaries=boundaries, equilibrium_solver=equilibrium_solver,
                         wavefunction_type=wavefunction_type)

        if not isinstance(syst, kwant.system.System):
            raise TypeError('"syst" must be a finalized kwant system')
        if not syst.leads:
            raise AttributeError("system has no leads.")
        if tmax is not None and boundaries is not None:
            raise ValueError("'boundaries' and 'tmax' are mutually exclusive.")
        if not _common.is_type(energy, 'real_number'):
            raise TypeError('energy must be a real number')
        if not _common.is_type(lead, 'integer'):
            raise TypeError('lead index must be an integer')
        if lead >= len(syst.leads):
            raise ValueError(
                "lead index must be smaller than {}.".format(len(syst.leads)))

        # get initial time and time argument name from the onebody wavefunction
        try:
            time_name = _common.get_default_function_argument(wavefunction_type,
                                                              'time_name')
            time_start = _common.get_default_function_argument(wavefunction_type,
                                                               'time_start')
        except Exception:
            time_name = _common.time_name
            time_start = _common.time_start
            logger.warning('retrieving initial time and time argument name from',
                           'the onebody wavefunction failed, use default values: ',
                           '"time_name"={}, "time_start"={}'.format(time_name,
                                                                    time_start))

        # add initial time to the params dict
        tparams = add_time_to_params(params, time_name=time_name,
                                     time=time_start, check_numeric_type=True)

        if boundaries is None:
            if spectra is None:
                spectra = kwantspectrum.spectra(syst.leads, params=tparams)
            boundaries = leads.automatic_boundary(spectra, tmax)

        scattering_states = equilibrium_solver(
            syst, energy=energy, params=tparams)
        self._psi_st = scattering_states(lead)
        self._wavefunction = functools.partial(wavefunction_type, syst=syst,
                                               boundaries=boundaries, params=params)
        self.energy = energy
        self.lead = lead
