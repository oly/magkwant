import functools
import numpy as np
import scipy
from quantum_dynamics import _kernels

__all__ = ['Coupled', 'DeCoupled', 'Huen']


def normalized(array):
    norm = scipy.linalg.norm(array, axis=1, keepdims=True)
    return np.divide(array, norm, where=(norm != 0))


class Coupled:
    """Solve the time-dependent Schrödinger equation coupled to the Landau Lifshitz
    Gilbert equation using either `scipy.integrate` or `Huen` method.

    If the ``time_step`` is not specified, the 'dopri5'
    integrator of `scipy.integrate` is called.
    If a ``time_step`` is provided, then the two step
    `Huen` method is used to evolve the coupled wave-function
    magnetization vector.

    Parameters
    ----------
    kernel : `magkwant.quantum_dynamics._kernels.CoupledScipy`
        A kernel that couples the rhs of both Schrödinger equation and
        Landau Lifshitz Gilbert equation.
    integrator : `scipy.integrate._ode.IntegratorBase`, default: dopri5
        The integrator to use with this solver.
    time_step : float
        The time step to be used for the `Huen` method integrator.
    **integrator_options
        Options to pass to the `scipy.integrate` integrator
        when instantiating the integrator. This used only for
        the scipy integrator.

    See Also
    --------
    scipy.integrate.ode
    """

    _default_options = {'atol': 1E-5, 'rtol': 1E-5, 'nsteps': int(1E9)}

    def __init__(self, kernel, time_step=None, **integrator_options):
        self.kernel = kernel
        # allocate storage for kernel output
        self._rhs_out = np.empty((kernel.coupling_size,), dtype=complex)
        self.time_step = time_step

        options = dict(self._default_options)
        options.update(integrator_options)

        self._integrator = scipy.integrate._ode.dopri5(**options)

        # Factor 2 because Scipy integrators expect real arrays
        self._integrator.reset(2 * self.kernel.coupling_size, has_jac=False)

    def _rhs(self, t, y, source=None):
        # Kernel expects complex, Scipy expects real
        self.kernel.rhs(y.view(complex), self._rhs_out, t, source=source)
        if self.time_step is None:
            return self._rhs_out.view(float)
        return self._rhs_out

    def __call__(self, psi_m, time, next_time, source=None):
        modified_rhs = self._rhs
        if source is not None:
            modified_rhs = functools.partial(self._rhs, source=source)
        if time == next_time:
            return psi_m
        # psi_m is complex, Scipy expects real
        if self.time_step is None:
            next_psi_m, final_time = self._integrator.run(
                modified_rhs, lambda: None, psi_m.view(float), time, next_time, (), ())
            if not self._integrator.success:
                raise RuntimeError('Integration failed between {} and {}'
                                   .format(time, next_time))
            assert final_time == next_time
            return next_psi_m.view(complex)

        next_psi_m, final_time = Huen(modified_rhs, self.time_step)(
            psi_m, time, next_time)

        return next_psi_m


class DeCoupled:
    """Solve the time-dependent Schrödinger equation
    using the two step `Huen` method.
    No coupling is assumed here. The coupling shall be done
    within coupled_manybody.


    Parameters
    ----------
    kernel : `magkwant.quantum_dynamics._kernels.CoupledScipy`
        A kernel that couples the rhs of both Schrödinger equation and
        Landau Lifshitz Gilbert equation.
    time_step : float
        The time step to be used for the `Huen` method integrator.

    """

    def __init__(self, kernel, time_step=None):
        self.kernel = kernel
        # allocate storage for kernel output
        self._rhs_out = np.empty((kernel.size,), dtype=complex)
        self.time_step = time_step

    def _rhs(self, t, y, m=None):
        # Kernel expects complex, Scipy expects real
        self.kernel.rhs(y, self._rhs_out, t, m=m)
        return self._rhs_out

    def __call__(self, psi, time, next_time, m=None):
        modified_rhs = self._rhs
        if m is not None:
            modified_rhs = functools.partial(self._rhs, m=m)
        if time == next_time:
            return psi

        next_psi, final_time = Huen(modified_rhs, self.time_step)(
            psi, time, next_time)

        return next_psi


class Huen:
    """Two step Huen method solver.

    Parameters
    ----------
    rhs : `magkwant.quantum_dynamics._kernels.CoupledScipy`
    time_step : float
        The step of the evolution.

    """

    def __init__(self, rhs, time_step):
        self.time_step = time_step
        self.rhs = rhs

    def _functional(self, rhs):
        """Two step Huen method functional of rhs.

        Returns
        -------
        A Lambda expression of the parameters
        ``time`` : instant of evolution,
        ``psi_m`` : the coupled array and
        ``dt`` : the time step.

        """
        return lambda time, psi_m, dt: (
            lambda k1: (
                lambda k2: (k1 + k2)/2
            )(dt * rhs(time + dt, psi_m + k1))
        )(dt * rhs(time, psi_m))

    def __call__(self, psi_m, time, next_time):
        t = time
        y = psi_m
        while t < next_time:
            y_prime = normalized(y + self.rhs(t, y) * self.time_step)
            t, y = t + self.time_step, normalized(
                y + .5 * self.time_step * (self.rhs(t, y) + self.rhs(t, y_prime)))
        return y, t
