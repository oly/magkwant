"""Calculations of LLG-tkwant manybody states.
The main classe inherits from tkwant.manybody.
Some doc strings from tkwant are used.
"""
import collections.abc
import functools as ft

import numpy as np
import tinyarray as ta
import kwant
import tkwant
import kwantspectrum

import quantum_dynamics.coupled_onebody as onebody
from tkwant import leads,  mpi, _common, _logging
from tkwant.system import add_time_to_params

from tkwant.manybody import lead_occupation, calc_intervals, combine_intervals, Interval, calc_energy_cutoffs


__all__ = ['calc_initial_state', 'WaveFunction', 'State']


# set module logger
logger = _logging.make_logger(name=__name__)
log_func = _logging.log_func(logger)

# default spin matrices
pauli_x = np.array([[0, 1], [1, 0]])
pauli_y = np.array([[0, -1j], [1j, 0]])
pauli_z = np.array([[1, 0], [0, -1]])


def normalized(array):
    norm = np.linalg.norm(array, axis=1, keepdims=True)
    return np.divide(array, norm, where=(norm != 0))


@log_func
def calc_initial_state(syst, tasks, boundaries, params=None,
                       scattering_state_type=onebody.DeCoupledScatteringStates,
                       mpi_distribute=mpi.round_robin, comm=None):
    """Calculate the initial manybody scattering wave function using MPI.

    Parameters
    ----------
    syst : `kwant.builder.FiniteSystem`
        The low level system for which the wave functions are to be
        calculated.
    boundaries : sequence of `~tkwant.leads.BoundaryBase`
        The boundary conditions for each lead attached to ``syst``.
    tasks : sequence of `tkwant.onebody.Task`
        Each element in the sequence represents a one-body state
        that composes the manybody state.
        An element of ``tasks`` must have at least the following three attributes:

            - `lead` : int, lead index
            - `mode` : int, scattering mode index
            - `energy` : float, energy
    params : dict, optional
        Extra arguments to pass to the Hamiltonian of ``syst``,
        excluding time.
    scattering_state_type : `tkwant.onebody.ScatteringStates`, optional
        Class to calculate time-dependent onebody wavefunctions starting in an
        equilibrium scattering state.
    mpi_distribute : callable, optional
        Function to distribute the tasks dict keys over all MPI ranks.
        By default, keys must be integer and are distributed round-robin like.
    comm : `mpi4py.MPI.Intracomm`, optional
        The MPI communicator over which to parallelize the computation.
        By default, use the tkwant global MPI communicator.
    magnet : `magkwant.textures.General`
            Holds all the information about the magnetic system.
        Jsd : float
            The sd coupling between the classical magnetic vectors
            and the the electronic spins.
        spin_basis : dict
            The spin matrices used in add the sd interaction to the
            Hamiltonion. All spin matrices should have the same shape
            as the onsite potentials used to define the tkwant system.

    Returns
    -------
    psi_init : dict of `tkwant.onebody.WaveFunction`
        Ensemble of all one-body scattering states that form the initial
        manybody state. Each one-body state stored in the ``psi_init`` dictionary
        corresponds to an element in the ``tasks`` list.
        The list index of ``tasks`` serves as dict key.
    """
    def scattering_state(energy, lead):
        """Calculate a one-body scattering state that can be evolved in time"""
        logger.debug(
            'calc scattering state: energy={}, lead={}'.format(energy, lead))
        try:
            return scattering_state_type(syst, energy, lead,
                                         params=params, boundaries=boundaries)
        except Exception:
            raise RuntimeError('scattering state calculation failed for '
                               'energy={}, lead={}'.format(energy, lead))

    if not isinstance(syst, kwant.system.System):
        raise TypeError('"syst" must be a finalized kwant system')
    comm = mpi.get_communicator(comm)
    tasks_per_rank = mpi.distribute_dict(tasks, mpi_distribute, comm)
    return {key: scattering_state(task.energy, task.lead)[task.mode]
            for key, task in tasks_per_rank.items()}


def calc_source(psi_init, d_x, d_y, d_z, Jsd, length):
    """
    Computes the mean field spin density at ``time``.
    It shall be used to evolve all one-body states.

    Parameters
    ----------
    time : int or float
        time argument up to which the solver should be evolved.
    """
    s_x = 0
    s_y = 0
    s_z = 0
    for _, onebody_psi in psi_init.items():
        scatt = onebody_psi.psi()
        s_x += d_x(scatt)
        s_y += d_y(scatt)
        s_z += d_z(scatt)
    source = np.array((s_x, s_y, s_z)).T / length
    return Jsd * normalized(source)


class WaveFunction(tkwant.manybody.WaveFunction):
    """Evolve a many-particle wavefunction in time."""

    def __init__(self, psi_init, d_x, d_y, d_z, tasks, magnet=None, Jsd=0, comm=None):
        """
        Initialize the manybody state.

        Parameters
        ----------
        psi_init : dict of `tkwant.onebody.WaveFunction`
            Dictionary with all initial one-body states.
            For load balancing the dictionary should be distributed over
            all MPI ranks.
        tasks : dict of `tkwant.onebody.Task`
            Dictionary containing the weighting factor for each one-body state.
            Each item must have at least the following attribute:

                - `weight` : `~numpy.ndarray`, weighting factor

            ``tasks`` must include all one-body states stored in `psi_init`
            and must be the same on all MPI ranks.
        comm : `~mpi4py.MPI.Intracomm`, optional
            The MPI communicator over which to parallelize the computation.
            By default, use the tkwant global MPI communicator.
        d_x : `kwant.operator.Density`
            Spin density operator along x (in spin space).
            Called with a ket it gives the corresponding expectation value of
            the projection of the generalized Pauli vector along x.
        d_y : `kwant.operator.Density`
            Spin density operator along y (in spin space).
            Called with a ket it gives the corresponding expectation value of
            the projection of the generalized Pauli vector along y.
        d_z : `kwant.operator.Density`
            Spin density operator along z (in spin space).
            Called with a ket it gives the corresponding expectation value of
            the projection of the generalized Pauli vector along z.
        """

        comm = mpi.get_communicator(comm)
        self.tasks = tasks
        self.length = len(psi_init)
        self.d_x = d_x
        self.d_y = d_y
        self.d_z = d_z
        self.Jsd = Jsd
        self.comm = comm
        self.psi = mpi.DistributedDict(psi_init, comm)

        # initialize the LLG solver and get the time step
        self.magnet = magnet
        initial_m = magnet.m_initial
        self.next_m = initial_m
        self.dt = magnet.time_step

        # initial the spin density matrix
        self.next_source = calc_source(
            psi_init, d_x, d_y, d_z, self.Jsd, self.length)

        self.time = 0

    def evolve_llg(self, m_init, t_start, source):
        """Evolve the magnetization configuration m_init
        by a single time step considering the spin density
        source.
        """
        self.magnet.set_source(source)
        self.magnet.initial_m = m_init
        return self.magnet.evolve(t_start + self.dt)

    def evolve_wfs(self, t_start, m):
        """
        Evolve all wavefunctions by a single time step in the
        presence of an sd term defined by m and return the corresponding
        spin density.

        Returns
        -------
        numpy.ndarray
            spin density matrix.

        """
        kept = {}
        for key, onebody_psi in self.psi.local_data().items():
            onebody_psi.evolve(t_start + self.dt, m)
            # We keep the state after evolving.
            kept[key] = onebody_psi
        return calc_source(kept, self.d_x, self.d_y, self.d_z, self.Jsd, self.length)

    def evolve(self, time, _texture=None):
        """
        Evolve all wavefunctions up to ``time``.

        Parameters
        ----------
        time : int or float
            time argument up to which the solver should be evolved
        """
        t = self.time
        s = self.next_source
        m = self.next_m
        while t < time:
            t = t + self.dt
            s = self.evolve_wfs(t, m)
            m = self.evolve_llg(m, t, source=s)

        self.next_m = m
        self.next_source = s
        self.time = time
        if _texture is True:
            return (m, s)

    def texture(self, time, return_spin_densities=None):
        """ Gives the magnetization texture at ``time``
        If return_spin_densities is set to True then a tuple of
        magnetization array and spin density is returned.

        """
        m, s = self.evolve(time, _texture=True)
        if return_spin_densities is None:
            return m
        return (m, s)


class State(tkwant.manybody.State):
    """Solve the time-dependent many-particle Schrödinger equation."""

    def __init__(self, syst, magnet, Jsd=0, spin_basis=None, tmax=None,
                 occupations=None, params=None, spectra=None, boundaries=None,
                 intervals=Interval, refine=True, combine=False, error_op=None,
                 scattering_state_type=onebody.DeCoupledScatteringStates,
                 manybody_wavefunction_type=WaveFunction, mpi_distribute=mpi.round_robin, comm=None):
        r"""
        Parameters
        ----------
        syst : `kwant.builder.FiniteSystem`
            The low level system for which the wave functions are to be
            calculated.
        tmax : float, optional
            The maximum time up to which to simulate. Sets the boundary
            conditions such that they are accurate up to ``tmax``.
            Must be set if ``boundaries`` are not provided.
            Mutually exclusive with `boundaries`.
        occupations : `tkwant.manybody.Occupation` or sequence thereof, optional
            Lead occupation. By default (or if ``occupations`` is set to `False`),
            all leads are taken into account and are
            considered as equally occupied with:
            chemical potential :math:`\mu = 0`, temperature :math:`T = 0`
            and the non-interacting Fermi-Dirac distribution as
            distribution function :math:`f(E)`.
            To change the default values (:math:`\mu, T, f(E)`),
            a `tkwant.manybody.Occupation` instance
            is precalculated with `tkwant.manybody.lead_occupation`
            and passed as ``occupations`` argument. If ``occupations`` is
            only one element, respectively a sequence with only one element,
            (:math:`\mu, T, f(E)`) will be identical in each lead.
            In the most general case, if (:math:`\mu, T, f(E)`)
            is different for each lead, ``occupations`` must be
            a sequence of `tkwant.manybody.Occupation` instances
            with an ordering similar to ``syst.leads``.
            In that case, ``occupations`` must have the same
            length as ``syst.leads``, respectively ``spectra``.
            A lead is not considered, if the corresponding ``occupations``
            element is set to `False`. Otherwise, for a lead to be
            considered, an element of the ``occupations`` sequence must have
            at least the following attributes:

            - `energy_range` : energy integration range
            - `bands` : int or list of int, bands (*n*) to be considered,
              all bands considered if `None`
            - `distribution` : callable, distribution function.
              Calling signature: `(energy)`.

        params : dict, optional
            Extra arguments to pass to the Hamiltonian of ``syst``,
            excluding time.
        spectra : sequence of `~kwantspectrum.spectrum`, optional
            Energy dispersion :math:`E_n(k)` for the leads. Must have
            the same length as ``syst.leads``. If needed but not present,
            it will be calculated on the fly from `syst.leads`.
        boundaries : sequence of `~tkwant.leads.BoundaryBase`, optional
            The boundary conditions for each lead attached to ``syst``.
            Must have the same length as ``syst.leads``.
            Mutually exclusive with ``tmax``.
        intervals : `tkwant.manybody.Interval` sequence or class, optional
            Momentum intervals and quadrature rules on these intervals.
            If ``intervals`` is a sequence, it represents
            the momentum intervals.
            In that case, initial integration intervals are not calculated
            from ``occupations`` but ``intervals`` is used instead.
            Each element of the ``intervals`` sequence must have at least
            the following attributes:

                - `lead` : int, lead index
                - `band` : int, band index (*n*)
                - `kmin` : float, lower momentum bound
                - `kmax` : float, upper momentum bound, must be larger `kmin`
                - `integration_variable` : string, energy vs. momentum integration
                - `order` : int, quadrature order
                - `quadrature` : string, quadrature rule to use.
                  See `tkwant.integration.calc_abscissas_and_weights`

            If ``intervals`` is a (data) class, it is passed to the
            `tkwant.manybody.calc_intervals` routine as ``Interval`` argument.
            By default, intervals are calculated from
            `tkwant.manybody.calc_intervals` and `tkwant.manybody.Interval`.
        refine : bool, optional
            If `True`, intervals are refined at the initial time.
        combine : bool, optional
            If `True`, intervals are grouped by lead indices.
        error_op : callable or `kwant.operator`, optional
            Observable used for the quadrature error estimate.
            Must have the calling signature of `kwant.operator`.
            Default: Error estimate with density expectation value.
        scattering_state_type : `tkwant.onebody.ScatteringStates`, optional
            Class to calculate time-dependent onebody wavefunctions starting in
            an equilibrium scattering state. Name of the time argument and
            initial time are taken from this class. If this is not possible,
            default values are used as a fallback.
        manybody_wavefunction_type : `tkwant.manybody.WaveFunction`, optional
            Class to evolve a many-particle wavefunction in time.
        mpi_distribute : callable, optional
                Function to distribute the tasks dict keys over all
                MPI ranks. By default, keys must be integer and are
                distributed round-robin like.
        comm : `mpi4py.MPI.Intracomm`, optional
            The MPI communicator over which to parallelize the computation.
        magnet : `magkwant.textures.General`
            Holds all the information about the magnetic system.
        Jsd : float
            The sd coupling between the classical magnetic vectors
            and the the electronic spins.
        spin_basis : dict
            The spin matrices used in add the sd interaction to the
            Hamiltonion. All spin matrices should have the same shape
            as the onsite potentials used to define the tkwant system.


        Notes
        -----
        The name of the time argument (`time_name`) and the initial time
        of the evolution (`time_start`) are taken from the default
        values of the `scattering_state_type.__init__` method. Changing the
        default values by partial prebind (e.g. via functools) is possible.
        """

        logger.info('initialize manybody.State')

        if not isinstance(syst, kwant.system.System):
            raise TypeError('"syst" must be a finalized kwant system')
        if tmax is None and boundaries is None:
            raise ValueError("'boundaries' or 'tmax' must be provided ")
        if tmax is not None and boundaries is not None:
            raise ValueError("'boundaries' and 'tmax' are mutually exclusive.")

        # get initial time and time argument name from the onebody wavefunction
        try:
            default_arg = _common.get_default_function_argument
            onebody_wavefunction_type = default_arg(scattering_state_type,
                                                    'wavefunction_type')
            time_name = default_arg(onebody_wavefunction_type, 'time_name')
            time_start = default_arg(onebody_wavefunction_type, 'time_start')
        except Exception:
            time_name = _common.time_name
            time_start = _common.time_start
            onebody_wavefunction_type = None
            logger.warning('retrieving initial time and time argument name from',
                           'the onebody wavefunction failed, use default values: ',
                           '"time_name"={}, "time_start"={}'.format(time_name,
                                                                    time_start))

        # add initial time to the params dict
        tparams = add_time_to_params(params, time_name=time_name,
                                     time=time_start, check_numeric_type=True)

        if spectra is None:
            spectra = kwantspectrum.spectra(syst.leads, params=tparams)
        if occupations is None:
            occupations = [lead_occupation()]
        if not isinstance(occupations, collections.abc.Iterable):
            logger.debug('occupation in all leads is {}'.format(occupations))
            occupations = [occupations]
        else:
            for i, occ in enumerate(occupations):
                logger.debug('occupation in lead={} is {}'.format(i, occ))
        if intervals is None:
            intervals = calc_intervals(spectra, occupations)
            if combine:
                intervals = combine_intervals(intervals)
        else:
            if not (isinstance(intervals, collections.abc.Iterable) or
                    isinstance(intervals, Interval)):
                intervals = calc_intervals(
                    spectra, occupations, interval_type=intervals)
            if combine:
                intervals = combine_intervals(intervals)
        try:
            len_int = len(intervals)
        except TypeError:
            len_int = 0
        if len_int == 0:
            logger.warning('no occupied states found, the chemical potential '
                           'is probably wrong.')
        else:
            logger.info('initial number of intervals={}'.format(len_int))
        for interval in intervals:
            logger.debug(interval)
        if boundaries is None:
            emin, emax = calc_energy_cutoffs(occupations)
            boundaries = leads.automatic_boundary(spectra, tmax,
                                                  emin=emin, emax=emax)

        self.time = time_start
        self.syst = syst
        self.spectra = spectra
        self.boundaries = boundaries
        self.occupations = occupations
        self.mpi_distribute = mpi_distribute
        self.onebody_wavefunction_type = onebody.DeCoupledWaveFunction
        self.manybody_wavefunction_type = manybody_wavefunction_type
        self.scattering_state_type = scattering_state_type

        self.magnet = magnet
        self.Jsd = Jsd
        self.spin_basis = spin_basis

        if spin_basis is None:
            self.spin_x = pauli_x
            self.spin_y = pauli_y
            self.spin_z = pauli_z
        else:
            self.spin_x = spin_basis["x"]
            self.spin_y = spin_basis["y"]
            self.spin_z = spin_basis["z"]

        self.d_x = kwant.operator.Density(self.syst, self.spin_x)
        self.d_y = kwant.operator.Density(self.syst, self.spin_y)
        self.d_z = kwant.operator.Density(self.syst, self.spin_z)

        # no public params attribute exists for the manybody state.
        # each individual one-body state holds its own parameters
        # the private params should be only used to create new inital
        # onebody states.
        self._params = params

        self._tasks_from_interval = {}
        tasks = self._calc_tasks(intervals)

        self.comm = mpi.get_communicator(comm)

        psi_init = self._calc_initial_state(tasks, self.comm)
        self.manybody_wavefunction = manybody_wavefunction_type(psi_init, self.d_x, self.d_y, self.d_z,
                                                                tasks, self.magnet, self.Jsd, self.comm)

        # by default, return the result of the higher-order rule, which in
        # our convention has to be the last element of the weight array
        self.return_element = -1

        if error_op is None:
            logger.info('set default error estimate based on density')
            error_op = kwant.operator.Density(syst)
        else:
            logger.info('set error estimate based on user given operator')
        self.error_op = error_op

        if refine:
            self.refine_intervals()

        logger.info('manybody.State initialization done')

    def _calc_initial_state(self, tasks, comm):
        """Calculate the initial manybody state for all tasks.

        Parameters
        ----------
        tasks : dict
            Dict with all tasks. Each item represents
            a one-body state that composes the manybody state.
        comm : `mpi4py.MPI.Intracomm`

        Returns
        -------
        psi_init : dict
            Ensemble of all one-body scattering states that form the
            initial manybody state.
        """
        return calc_initial_state(self.syst, tasks, self.boundaries,
                                  self._params, self.scattering_state_type,
                                  mpi_distribute=self.mpi_distribute, comm=comm)

    def texture(self, time, return_spin_densities=None):
        """Calculate the magnetization texture
        affected by the many-body spin density.
        """
        return self.manybody_wavefunction.texture(time, return_spin_densities)
