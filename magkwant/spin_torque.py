import numpy as np
import tinyarray as ta
import fields.m_cross_j_scalar_nabla as m_j_nabla
import fields.gradient as nabla


def torque(m, d_xyz, je=ta.array([1, 0, 0]), alpha=1, beta=1, u=1):
    """Computes the spin transfer torque to be added
    to the rhs of the LLG equation.
    Parameters
    ----------
    m : ndarray
       The magnetization vector.
    d_xyz : ndarray
       Matrix of the heads of the gradient.
    alpha : float, optional
        The damping constant.
        If it is not given then the damping constant
        of the instance `texture` is used.
    beta : float
        The non-adiabaticity parameter
    u : float
        Proportional to the applied current.
    see ref [1].

    [1] Phys. J. B 59, 429–433 (2007).
    """
    _je = np.array(je, dtype=float)
    c_field = - u * (alpha - beta) / (1 + alpha**2)
    c_damping = u * (1 + alpha * beta) / (1 + alpha**2)
    field_like = m_j_nabla.compute(m, d_xyz, _je)
    return c_field * field_like + c_damping * np.cross(m, field_like)


def drive(texture, je=ta.array([1, 0, 0]), beta=0.1, u=1, alpha=None):
    """
    Parameters :
    ----------
    texture : `magkwant.textures.General`
        The predefined texture to move using ``torque``.
    je : ndarray
        The unit vector of the current direction.
    alpha : float, optional
        The damping constant.
        If it is not given then the damping constant
        of the instance `texture` is used
    beta : float
        The non-adiabaticity parameter
    u : float
        Proportional to the applied current
    see ref [1].

    [1] Phys. J. B 59, 429–433 (2007).

    returns :
    -------
        magkwant.textures.General
        The texture to be evolved in time.
    exemple of use:
    -------
        >>> texture = magkwant.textures.General()
        >>> driven = magkwant.spin_torque.drive(texture)
        to obtain the driven texture at time we call:
        >>> driven(time)
    """
    sys = texture.sys
    d_xyz = nabla.heads(sys)
    dimension = d_xyz.shape[1]
    three_dim = [0 for i in range(3 - dimension)]
    # complement the current vector to 3d
    current = np.append(je, three_dim)

    if alpha is None:
        alpha = texture.damping

    def _torque(time, m):
        return torque(m, d_xyz, current, alpha, beta, u)
    texture.set_torque(_torque)

    return texture
