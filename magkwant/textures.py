import functools
import numpy as np
import scipy
from quantum_dynamics._solvers import Huen
from fields.common_fields import get_fields
from fields.supplemental_fields import get_4spin, get_others
from fields.interactions import Main_interactions, _inspect
from fields.supplemental_interactions import Triangular_interactions


__all__ = ["General", "Triangular", "Square"]


def normalized(array):
    norm = scipy.linalg.norm(array, axis=1, keepdims=True)
    return np.divide(array, norm, where=(norm != 0))


def get_m0(sys, magparams):
    # TODO : make it work correctly when shape is used
    sys_size = len(sys.H)
    m_initial = np.zeros((sys_size, 3), dtype=float)
    for k, (tail, hvhv) in enumerate(sys.H.items()):
        value = hvhv[1]
        if callable(value):
            if magparams:
                m_initial[k, :] = value(tail, **magparams)
            else:
                m_initial[k, :] = value(tail)
        else:
            m_initial[k, :] = value
    return normalized(m_initial)


class General(Main_interactions):
    r"""Computes the magnetization texture on a general kwant system.

    Parameters
    ----------
    sys : `kwant.builder.Builder`
    field_inputs: dict
        Field parameters, namely:
        demag: demagnetization strength (magnetic dipole strength),
        aniso: anisotropy vector.
        dmi: Dzyaloshinskii-Moriya Interaction strength.
        If `dmi` is given as a constant, then the default DMI vector
        :math: `D (z \times r_{ij})` is used.
        If a function is given, it should take two sites as parameters
        and return the corresponding DMI vector.
        The first argument corresponds to the site at which
        the `dmi` vector is evaluated.
    llg_constants: dict, default (None)
        The parameters to be used in the LLG equation solving.
        Contains:
        t_start: initial time,
        damping: damping constant
        rtol, atol, nsteps: ode integration options.
    magnetic_field: callable or ndarray
        Gives the uniform magnetic field through sys
        if callable it should be a function of time.
    magparams: dict, default (None)
        Gives a dictionary containing the parameters used in defining
        the vector field and corresponding hoppings on sys.

    Attributes
    ----------
    additional_field: callable, default (None)
        an additional field to pass to the ``rhs`` of LLG equation. Should be a
        function of time and magnetization respectively.
    added_torque: callable, default (None)
        damping torque, field like torque or a linear combination of the two.
        Should be given as a function of time and magnetization respectively.

    """

    def __init__(self, sys, field_inputs=None, m0=None,
                 llg_constants=None, magnetic_field=None, magparams=None):
        super().__init__(sys=sys, field_inputs=field_inputs, magparams=magparams)
        self.llg_locals = {"t_start": 0, "damping": 0.5, "atol": 1E-9,
                           "rtol": 1E-9, "nsteps": int(1E9), "time_step": .01}
        # check the inputs are valid
        if llg_constants is not None:
            _inspect(llg_constants, self.llg_locals)

        # ode solver options
        options = {"atol": 1E-9, "rtol": 1E-9, 'nsteps': int(1E9)}
        self.llg_constants = llg_constants
        if self.llg_constants is not None:
            self.llg_locals.update(self.llg_constants)

        for k, v in self.llg_locals.items():
            if k in options:
                options.update({k: v})

        self.damping = self.llg_locals["damping"]
        self.t_start = self.llg_locals["t_start"]
        self.time_step = self.llg_locals["time_step"]

        self.sys_size = len(self.sys.H)

        # get the initial magnetization
        if m0 is None:
            self.m_initial = get_m0(self.sys, self.magparams)
        else:
            self.m_initial = normalized(m0)

        self._shape = self.m_initial.shape
        self.next_m = self.m_initial.flatten("f")

        self.time = self.t_start
        self.magnetic_field = magnetic_field

        # ode solver for LLG equation
        self._integrator = scipy.integrate._ode.dopri5(**options)
        self._integrator.reset(3 * self.sys_size, has_jac=False)

        # additional terms to the rhs of LLG
        self._supplemental_field = None  # only for internal use
        self.additional_field = None  # function of time, and m
        self.added_torque = None  # function of time, and m

        # include the main interactions
        self.include_exchange_and_dmi()
        self.include_anisotropy()
        self.include_dipole()

    def set_torque(self, the_torque):
        self.added_torque = the_torque

    def effective_field(self, t, m):
        r"""Here we compute the rhs of llg :
        :math: `-1/(1+\lambda^2)(M \times H + \lambda  M \times (M \times H))`
        """
        reshaped_m = np.reshape(m, (3, self.sys_size)).T
        _field = get_fields(reshaped_m.real, self.neighbors, self.h_exch,
                            h_aniso=self.h_aniso, h_dmi=self.h_dmi, h_dipole=self.h_dipole, indices=self.indices)

        # add additional field
        if self.additional_field is not None:
            _field += self.additional_field(t, reshaped_m)

        # add supplemental field, only for internal use
        if self._supplemental_field is not None:
            _field += self._supplemental_field(t, reshaped_m)

        # add time dependent magnetic field
        if self.magnetic_field is not None:
            try:
                _field += self.magnetic_field
            except TypeError:
                _field += self.magnetic_field(t)
        return _field

    def rhs(self, t, m, temperature=0):
        reshaped_m = np.reshape(m, (3, self.sys_size)).T
        _effective_field = self.effective_field(t, m)
        prefactor = - 1 / (1 + self.damping**2)

        result = np.cross(reshaped_m, _effective_field) + \
            self.damping * np.cross(reshaped_m,
                                    np.cross(reshaped_m, _effective_field))

        if self.added_torque is not None:
            result += self.added_torque(t, reshaped_m)
        if temperature != 0:
            stochastic_factor = np.sqrt(
                2 * self.dampign * temperature / self.time_step)
            result += stochastic_factor * np.random.normal(size=self._shape)

        return prefactor * result.flatten("f")

    def equilibrate(self, temperature=0, num_steps=1e5, return_m=None):
        """Evolve the classical system up to equilibrium
        at `temperature` after a number of steps `num_step`.

        Parameters
        ----------
        temperature : int or float
            The classical system's temperature, it should be
            given in units of Hiesenberg hopping energy.
        num_steps : int
            The number of steps to consider for equilibrating
            the classical system at `temperature`
        return_m : bool
            Whether the magnetization vector should be returned
            or not.

        """
        _rhs = functools.partial(self.rhs, temperature=temperature)
        self.next_m, self.time = Huen(_rhs, self.time_step)(
            self.next_m, self.time, num_steps * self.time_step)
        self.time = self.t_start
        if return_m is True:
            return np.reshape(self.next_m, (3, self.sys_size)).T

    def stochastic_texture(self, temperature=0, num_steps=1e5):
        """Computes the magnetization's stochastic average within a window of
         `num_steps` after equilibration.

        Parameters
        ----------
        temperature : int or float
            The classical systems temperature, it should be
            given in units of Hiesenberg hopping energy.
        num_steps : int
            The number of steps to consider for averaging
            the magnetization at `temperature`

        """
        _rhs = functools.partial(self.rhs, temperature=temperature)
        tn = 0
        m_av = 0
        while tn < num_steps * self.time_step:
            tn += self.time_step
            self.next_m, self.time = Huen(_rhs, self.time_step)(
                self.next_m, self.time, tn)
            m_av += self.next_m
        self.time = self.t_start
        m = m_av / num_steps
        return np.reshape(m, (3, self.sys_size)).T

    def __call__(self, time):
        """Evolves the magnetization up to `time`."""
        if self.time_step is None:
            self.next_m, self.time = self._integrator.run(self.rhs, lambda: None,
                                                          self.next_m, self.time, time, (), ())
            return np.reshape(self.next_m, (3, self.sys_size)).T

        self.next_m, self.time = Huen(self.rhs, self.time_step)(
            self.next_m, self.time, time)
        return np.reshape(self.next_m, (3, self.sys_size)).T


def higher_order_heisenberg(t, M, h_bq, h_4spin, B2=0, B3=0, K=0, Y=0):
    result = 0
    if K != 0:
        result += get_4spin(M, h_4spin, K=K)
    if any(const != 0 for const in [B2, B3, Y]):
        result += get_others(M, h_bq, B2=B2, B3=B3, Y=Y)
    return result


class Triangular(General):
    r"""Computes the magnetization texture on a Triangular lattice
    with four spin and biquadratic interactions.

    Parameters
    ----------
    sys : `kwant.builder.Builder`
    field_inputs: dict
        gives the field parameters.
        Contains:
        demag: demagnetization strength (magnetic dipole strength),
        aniso: anisotropy vector :math: `v_{aniso}`
        dmi: Dzyaloshinskii-Moriya Interaction strength.
        If dmi is given as a constant, then the default DMI vector
        :math: `D (z \times r_{ij})` is used.
        If a function is given, it should take two sites as parameters and return
        the corresponding DMI vector.
        The first argument corresponds to the site at which
        the `dmi` vector is evaluated.
        K: four spin interaction strength
        Y: three spin interaction strength
        B2: bi-quadratic interaction strength
        B3: bicubic interaction
    llg_constants: dict, default (None)
        The parameters to be used in the LLG equation solving.
        Keys:
        t_start: initial time,
        damping: damping constant
        rtol: relative tolerance of the ode integrator, default (1E-6)
        atol: absolute tolerance of the ode integrator, default (1E-6)
        nsteps: maximum number of steps in the integration, default (1E9)
    magnetic_field: callable or ndarray
        Gives the uniform magnetic field through `sys`
        if callable it should be a function of `time`.
    magparams: dict, default (None)
        a dictionary containing the parameters used
        in defining the vector field and corresponding hoppings on `sys`.
    pbc: bool, default (False)
        Whether periodic boundary conditions are considered or not.
    true_triangular: bool, default (True)
        If set to False, a square lattice is considered (see the class Square).

    Attributes
    ----------
    additional_field: callable, default (None)
        an additional field to pass to the ``rhs`` of LLG equation. Should be a
        function of time and magnetization respectively.
    torque: callable, default (None)
        damping torque, field like torque or a linear combination of the two.
        Should be given as a function of time and magnetization respectively.

    Note
    ----
        If pbc is True then a periodic sys has to be given,
        otherwise, only the four spin and biquadratic interactions
        are computed with periodic boundary conditions.
    """

    def __init__(self, sys, field_inputs=None, m0=None, llg_constants=None,
                 magnetic_field=0, magparams=None, pbc=False, true_triangular=True):
        super().__init__(sys, field_inputs=field_inputs, m0=m0, llg_constants=llg_constants,
                         magnetic_field=magnetic_field, magparams=magparams)
        self.K = self.field_locals["K"]
        self.Y = self.field_locals["Y"]
        self.B2 = self.field_locals["B2"]
        self.B3 = self.field_locals["B3"]

        self.pbc = pbc
        self.true_triangular = true_triangular
        Site_matrices = Triangular_interactions(self.sys, self.field_locals,
                                                true_triangular=self.true_triangular, pbc=self.pbc)
        self.h_bq = Site_matrices.biquadratic()
        self.h_4spin = Site_matrices.four_spin()
        if any(const != 0 for const in [self.B2, self.B3, self.Y, self.K]):
            self._supplemental_field = functools.partial(higher_order_heisenberg, h_bq=self.h_bq,
                                                         h_4spin=self.h_4spin, B2=self.B2, B3=self.B3, K=self.K, Y=self.Y)


class Square(Triangular):
    r"""Computes the magnetization texture on a Square lattice
    with four spin and biquadratic interactions.

    Parameters
    ----------
    sys : `kwant.Builder`
    field_inputs: dict
        gives the field parameters.
        Keys:
        K: four spin interaction strength,
        B2: bi-quadratic interaction strength,
        demag: demagnetization strength (magnetic dipole strength),
        aniso: anisotropy vector :math: `v_{aniso}`
        dmi: Dzyaloshinskii-Moriya Interaction strength.
        If dmi is given as a constant, then the default DMI vector
        :math: `D (z \times r_{ij})` is used.
        If a function is given, it should take two sites as parameters and return
        the corresponding DMI vector.
        The first argument corresponds to the site at which
        the `dmi` vector is evaluated.
        K: four spin interaction
        Y: three spin interaction
        B2: biquadratic interaction
        B3: bicubic interaction
    llg_constants: dict, default (None)
        The parameters to be used in the LLG equation solving.
        Keys:
        t_start: initial time,
        damping: damping constant
        rtol: relative tolerance of the ode integrator, default (1E-6)
        atol: absolute tolerance of the ode integrator, default (1E-6)
        nsteps: maximum number of steps in the integration, default (1E9)
    magnetic_field: callable or ndarray
        Gives the uniform magnetic field through sys
        if callable it should be a function of time.
    magparams: dict, default (None)
        a dictionary containing the parameters used in defining the vector field and
        corresponding hoppings on `sys`.
    pbc: bool, default (False)
        Whether periodic boundary conditions are considered or not.

    Attributes
    ----------
    additional_field: callable, default (None)
        an additional field to pass to the ``rhs`` of LLG equation. Should be a
        function of time and magnetization respectively.
    torque: callable, default (None)
        damping torque, field like torque or a linear combination of the two.
        Should be given as a function of time and magnetization respectively.

    Note
    ----
        If pbc is True then a periodic sys has to be given,
        otherwise, only the four spin and biquadratic interactions
        are computed with periodic boundary conditions.
    """

    def __init__(self, sys, field_inputs=None, m0=None, llg_constants=None,
                 magnetic_field=0, magparams=None, pbc=False):
        super().__init__(sys, field_inputs=field_inputs, m0=m0, llg_constants=llg_constants,
                         magnetic_field=magnetic_field, magparams=magparams, pbc=pbc, true_triangular=False)
