cimport cython
import numpy as np

@cython.boundscheck(False)
@cython.wraparound(False)
def compute(double[:, :] M,  int[:, :] d_xyz, double[:] e_j):
    r'''Computes the quantity
        :math: `M \times (j_e \ cdot \nabla) M`
    to be used in computing the spin torque.

    Parameters
    ----------

    M : ndarray
        Array of magnetization vectors
    d_xyz : ndarray
        Array of heads to be used to compute
        the gradient of M in 3d.
        The first column contains the shifts along
        :math: `x` ...
    e_j : ndarray
        Unit vector along the direction of the applied current.

    Notes
    -----

    To illustrate how
        :math: `M \times (j_e \ cdot \nabla) M`
    is computed we consider a single site :math: `i`.
    First we compute
        :math: `j \cdot \nabla`
    as
        :math: `j_x (M^x_i - M_i) + j_y (M^y_i - M_i) + j_z (M^z_i - M_i)`
    where :math: `M^r_i`
    is the magnetic moment after a unit displacement from site :math: `i`
    in direction :math: `r`.
    Since we are interested in the product
        :math: `M_i \times (j \cdot \nabla)`
    one can omit the term proportional to :math: `M_i`
    then we are left with:
        :math: `M_i \times (j_x M^x_i + j_y M^y_i + j_z M^z_i)`

    '''

    cdef Py_ssize_t i
    cdef int x
    cdef int y
    cdef int z
    cdef double j_nabla_0
    cdef double j_nabla_1
    cdef double j_nabla_2
    cdef Py_ssize_t num_sites = d_xyz.shape[0]
    cdef Py_ssize_t dimension = d_xyz.shape[1]

    output = np.zeros((num_sites, 3), dtype=np.double)
    cdef double[:, :] output_view = output


    if dimension == 2:
        for i in range(num_sites):
            dx = d_xyz[i, 0] # the head site along positive :math: `x`
            dy = d_xyz[i, 1] # the head site along positive :math: `y`

            j_nabla_0 = e_j[0] * M[dx, 0] + e_j[1] * M[dy, 0]
            j_nabla_1 = e_j[0] * M[dx, 1] + e_j[1] * M[dy, 1]
            j_nabla_2 = e_j[0] * M[dx, 2] + e_j[1] * M[dy, 2]

            h_x = j_nabla_1 * M[i, 2] - j_nabla_2 * M[i, 1]
            h_y = j_nabla_2 * M[i, 0] - j_nabla_0 * M[i, 2]
            h_z = j_nabla_0 * M[i, 1] - j_nabla_1 * M[i, 0]

            output_view[i, 0] = h_x
            output_view[i, 1] = h_y
            output_view[i, 2] = h_z

    if dimension == 3:
        for i in range(num_sites):
            dx = d_xyz[i, 0]
            dy = d_xyz[i, 1]
            dz = d_xyz[i, 2] # the head site along positive :math: `z`

            j_nabla_0 = M[dx, 0] * e_j[0] + M[dy, 0] * e_j[1] + M[dz, 0] * e_j[2]
            j_nabla_1 = M[dx, 1] * e_j[0] + M[dy, 1] * e_j[1] + M[dz, 1] * e_j[2]
            j_nabla_2 = M[dx, 2] * e_j[0] + M[dy, 2] * e_j[1] + M[dz, 2] * e_j[2]

            h_x = j_nabla_1 * M[i, 2] - j_nabla_2 * M[i, 1]
            h_y = j_nabla_2 * M[i, 0] - j_nabla_0 * M[i, 2]
            h_z = j_nabla_0 * M[i, 1] - j_nabla_1 * M[i, 0]

            output_view[i, 0] = h_x
            output_view[i, 1] = h_y
            output_view[i, 2] = h_z

    if dimension == 1:
        for i in range(num_sites):
            dx = d_xyz[i, 0]
            j_nabla_0 = M[dx, 0] * e_j[0]

            h_y = - j_nabla_0 * M[i, 2]
            h_z = j_nabla_0 * M[i, 1]

            output_view[i, 0] = h_x
            output_view[i, 1] = h_y
            output_view[i, 2] = h_z


    return output
