"""
This module precomputes the sites directed from each lattice site.
It allows for computing the space gradient needed to evaluate the
torque resulting from an applied charge current.
"""
from collections import defaultdict
import numpy as np
import tinyarray as ta

__all__ = ["heads"]


def heads(sys):
    r"""Precomputes the sites directed from each lattice site.

    Parameters
    ----------
    sys : `kwant.builder.Builder`
        The kwant system used to define the magnet.

    Returns
    -------
        An array of site indices corresponding to the displacements
        in the existing directions.

    """
    labeled_tags = defaultdict(int)
    H_sys = sys.H.items()
    num_sites = len(H_sys)
    dimension = len(min(H_sys)[0].pos)
    vects = {}
    for i in range(dimension):
        emp = np.zeros(dimension, dtype=np.int32)
        emp[i] = 1
        vects[i] = ta.array(emp)
    for i, (tail, _) in enumerate(H_sys):
        labeled_tags[tail.tag] = i
    d_xyz = np.zeros((num_sites, dimension), dtype=np.int32)
    for i, (tail, _) in enumerate(H_sys):
        for j in range(dimension):
            shift = tail.tag + vects[j]
            if shift in labeled_tags:
                d_xyz[i, j] = labeled_tags[shift]
            else:
                d_xyz[i, j] = i
    return d_xyz
