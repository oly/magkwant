from collections import defaultdict
from itertools import islice
import numpy as np
import tinyarray as ta
import scipy

__all__ = ["Main_interactions"]


def default_dmi_vector(site_i, site_j, max_dist=1):
    """Computes the default DMI interaction:
    :math: `D_{ij} = z \times u_{ij}`
    with :math: `u_{ij}` the unit vector oriented from i to j.
    site_i : `kwant.system.Site`
        The site at which DMI is computed
    site_j : `kwant.system.Site`
        The site interacting with ``site_i``.
    max_dist : float
        The maximum separation distance from ``site_i`` to consider.
        It's used to eliminate the DMI from further sites
        attached to ``site_i`` for systems with periodic boundary
        conditions.
    """
    e_z = ta.array([0, 0, 1])
    u_ij = site_j.pos - site_i.pos
    norm_ij = np.linalg.norm(u_ij)
    if norm_ij <= max_dist:
        u_ij = np.divide(u_ij, norm_ij, where=(norm_ij != 0))
        return np.cross(e_z, u_ij)
    return 0 * e_z  # eliminate contributions from pbc sites


class _Neighbors_info:
    r"""Prepares the neighbors related information to be used in field calculations.

    Parameters
    ----------
    sys : `kwant.builder.Builder`
    field_inputs: dict
        The field parameters.
        Contains:
            demag: demagnetization strength (magnetic dipole strength),
            aniso: anisotropy vector :math: `v_{aniso}`
            dmi: Dzyaloshinskii-Moriya Interaction strength.
            If `dmi` is given as a constant, then the default DMI vector
            :math: `D (\bf{z} \cross \bf{r}_{ij})` is used.
            If a function is given, it should take two sites as parameters and return
            the corresponding DMI vector.
            The first argument corresponds to the site at which the `dmi` vector is evaluated.
            K: four spin interaction
            Y: three spin interaction
            B2: biquadratic interaction
            B3: bicubic interaction
    magparams: dict
        Contains the parameters used in defining the vector field and
        corresponding hoppings on `sys`.

    Attributes
    ----------
    neighbors_info: dict or 1darray default (None)
        If aniso is callable then a dictionnary of the form:
        {tail_i:[(head_j, hopping_j, dmi_{ij}, aniso_i), for j in existing neighbors of i] for i in sites}
        otherwise, it gives the anisotropy vector.
    num_sites: int
        Number of sites in the `sys`.
    """

    def __init__(self, sys, field_inputs, magparams):
        self.field_locals = {
            "aniso": None,
            "dmi": None,
            "K": None,
            "Y": None,
            "B2": None,
            "B3": None,
            "demag": None,
            "demag_cutoff": None}
        # check whether the inputs are valid
        if field_inputs is not None:
            _inspect(field_inputs, self.field_locals)

        self.field_inputs = field_inputs
        if self.field_inputs is not None:
            self.field_locals.update(self.field_inputs)
            # makes 0 and None inputs equivalent
            for key in ["K", "Y", "B2", "B3"]:
                if self.field_locals[key] == None:
                    self.field_locals.update({key: 0})
            for key in ["aniso", "dmi", "demag", "demag_cutoff"]:
                if self.field_locals[key] == 0:
                    self.field_locals.update({key: None})

        self.demag = self.field_locals["demag"]
        self.demag_cutoff = self.field_locals["demag_cutoff"]
        self.dmi = self.field_locals["dmi"]
        self.aniso = self.field_locals["aniso"]

        self.sys = sys
        self.magparams = magparams
        order = defaultdict(int)
        for i, (tail, hvhv) in enumerate(self.sys.H.items()):
            order[tail] = i

        self.neighbors_info = defaultdict(list)

        fdmi = default_dmi_vector
        if callable(self.dmi):
            fdmi = self.dmi

        # fill the dictionary neighbors_info
        if not callable(self.aniso):
            if self.aniso is not None:
                aniso = correct_aniso(self.aniso)
                #assert is_vector(self.aniso)
                self.anisotropy_info = np.zeros(3)
                for aniso_i in aniso:
                    k_a = np.linalg.norm(aniso_i)
                    if k_a != 0:
                        # extract the anisotropy unit vector
                        aniso_vector = aniso_i / k_a
                        # the anisotropy is a constant vector
                        self.anisotropy_info += 2 * k_a * aniso_vector * aniso_vector
            for i, (tail, hvhv) in enumerate(self.sys.H.items()):
                heads = islice(hvhv, 2, None, 2)
                for head in heads:
                    value = self.sys.__getitem__((tail, head))
                    val = value
                    if callable(value):
                        val = value(tail, head, **self.magparams)
                    self.neighbors_info[i].append(
                        (order[head], val, fdmi(tail, head)))
        else:
            # the anisotropy is site dependent and is given as a dictionary of the form
            # {site_id: anisotropy field}
            self.anisotropy_info = {}
            for i, (tail, hvhv) in enumerate(self.sys.H.items()):
                heads = islice(hvhv, 2, None, 2)
                aniso = correct_aniso(self.aniso(tail))
                res = np.zeros(3)
                for aniso_i in aniso:
                    k_a = np.linalg.norm(aniso_i)
                    if k_a != 0:
                        # extract the anisotropy unit vector
                        aniso_vector = aniso_i / k_a
                        # the anisotropy is a constant vector
                        res += 2 * k_a * aniso_vector * aniso_vector
                self.anisotropy_info[i] = res
                for head in heads:
                    value = self.sys.__getitem__((tail, head))
                    val = value
                    if callable(value):
                        val = value(tail, head, **self.magparams)
                    self.neighbors_info[i].append(
                        (order[head], val, fdmi(tail, head)))
        try:
            # get maximum number of neighbors
            self.num_neighbors = max(map(len, self.neighbors_info.values()))
        except ValueError:
            self.num_neighbors = 0
        self.num_sites = len(order)  # number of sites


class Main_interactions(_Neighbors_info):
    r"""Precomputes the effective fields.
    Only the information concerning the sites connections are computed.
    This assumes stationnary interactions, homogenous throughout `sys`.
    The classical magnetic Hamiltonian is given by:
    :math: `H_i = - \sum_{j} \{ Jij S_i \cdot S_j \
                                 + (v_{aniso} \cdot S_i) ^ 2 \
                                 + S_i \dot (D_{ij} \cross S_j) \
                                 + d ((S_j \cdot r_{ij}) r_{ij} / r_{ij}^5 - S_j/r_{ij}^3) \}`

    Parameters
    ----------
    sys : `kwant.builder.Builder`
    field_inputs: dict
        The field parameters.
        Contains:
            demag: float, default (None)
                demagnetization strength (magnetic dipole strength).
            aniso: callable or 1darray, default (None)
                anisotropy vector :math: `v_{aniso}`, it can be a vector or
                a site dependent function
            dmi: callable or float
                Dzyaloshinskii-Moriya Interaction strength.
            If `dmi` is given as a constant, then the default DMI vector
            :math: `D (z \times r_{ij})` is used.
            If a function is provided, it should take two sites as parameters and return
            the corresponding DMI vector.
            The first argument corresponds to the site at which the `dmi` vector is evaluated.
            K: four spin interaction
            Y: three spin interaction
            B2: biquadratic interaction
            B3: bicubic interaction
    magparams: dict, default (None)
        Contains the parameters used in defining the vector field and
        hoppings on `sys`.

    Attributes
    ----------
    neighbors: numpy.ndarray
        Array of connected sites up to an arbitrary far neighbor hoppings in sys.
        Each row gives the sites connected to the site underlying the row indices.
    h_exch: numpy.ndarray
        The hopping integrals for each site.
    h_dmi: numpy.ndarray, default (None)
        Each row stocks the DMI vectors corresponding to the underlying site indices.
    h_aniso: numpy.ndarray, default (None)
        Contains the onsite anisotropy vectors.
    h_dipole: numpy.ndarray, default (None)
        The prefactor of the demagnetization field defined as:
        :math: `h_{dipole} = \frac{d}{r_{ij}^3}\left(\frac{3 r_{ij}^2}{r_{ij}^2} - 1 \right)`
    """

    def __init__(self, sys, field_inputs=None, magparams=None):
        super().__init__(sys=sys, field_inputs=field_inputs, magparams=magparams)
        # fill the bilinear heisenberg sites and hoppings
        # to be done in any case
        self.neighbors = np.zeros(
            (self.num_sites, self.num_neighbors), dtype=np.int32)
        self.h_exch = np.zeros(
            (self.num_sites, self.num_neighbors), dtype=float)

        # to fill only if the interactions are requested
        self.h_aniso = None
        self.h_dmi = None
        self.h_dipole = None
        self.indices = None  # indices of sites within a cutoff

        self.dim = len(min(self.sys.H.items())[0].pos)

        if self.dmi is not None:
            self.h_dmi = np.zeros(
                (self.num_sites, self.num_neighbors, 3), dtype=float)
        if self.aniso is not None:
            self.h_aniso = np.zeros((self.num_sites, 3), dtype=float)
        if self.demag is not None:
            self.h_dipole = np.zeros(
                (self.num_sites, self.num_sites, 3), dtype=float)

    def include_exchange_and_dmi(self):
        """Computes exchange interaction and dmi"""
        if self.dmi is None:
            # only the exchange is computed
            for i, info in self.neighbors_info.items():
                for (j, (k, hopping, dmi_ij)) in enumerate(info):
                    ######### exchange #########
                    self.neighbors[i, j] = k
                    self.h_exch[i, j] = hopping
        else:
            # compute for dmi and exchange at the same time
            if callable(self.dmi):
                dmi_constant = 1
            else:
                dmi_constant = self.dmi
            for i, info in self.neighbors_info.items():
                for (j, (k, hopping, dmi_ij)) in enumerate(info):
                    ######### exchange #########
                    self.neighbors[i, j] = k
                    self.h_exch[i, j] = hopping
                    ############ DMI ###########
                    self.h_dmi[i, j, :] = dmi_constant * dmi_ij

    def include_anisotropy(self):
        """Computes the anisotropy matrix"""
        if self.aniso is not None:
            if callable(self.aniso):
                for i, info in self.anisotropy_info.items():
                    ######### anisotropy #########
                    self.h_aniso[i, :] = info
            else:
                self.h_aniso = np.repeat(
                    self.anisotropy_info[None, :], self.num_sites, axis=0)

    def include_dipole(self):
        # TODO: make it performant via FFT
        """Computes the demagnetization matrix"""
        if self.demag is not None:
            if self.demag_cutoff is not None:
                self.indices, r_ij = r_ij_cutoff(
                    self.sys, r_c=self.demag_cutoff)
            else:
                pos_matrix = np.zeros((self.num_sites, 3), dtype=float)
                for i, (tail, _) in enumerate(self.sys.H.items()):
                    pos_matrix[i, :self.dim] = ta.array(tail.pos)
                r_ij = pos_matrix[:, None, :] - pos_matrix[None, :, :]
            d_ij = scipy.linalg.norm(r_ij, axis=2, keepdims=True)
            r_ij = np.divide(r_ij, d_ij, where=(d_ij != 0))
            self.h_dipole = self.demag * \
                np.divide(3 * r_ij ** 2 - 1, d_ij ** 3, where=(d_ij != 0))


def r_ij_cutoff(sys, r_c):
    """Computes the site to site vectors within
    a circle of radius ``r_c``.

    Parameters
    ----------
    sys : `kwant.builder.Builder`
    r_c : int
        The maximum inter-site distance to consider
        in evaluating the demagnetization field.
    """
    assert isinstance(r_c, int)
    sites = sys.sites()
    id_by_pos = {si.pos: k for k, si in enumerate(sites)}
    ref_site = min(sites)
    dim = len(ref_site.pos)
    num_tails = len(sites)
    num_heads = (2 * r_c + 1)**dim + 1

    rij_matrix = np.zeros((num_tails, num_heads, 3), dtype=float)
    # holds the indices corresponding to the heads
    indices = np.zeros((num_tails, num_heads), dtype=np.int32)
    indices -= 11
    # sites marked by -11 correspond to sites
    # out of the physical system.

    def valid_closest(neigbrs):
        """Filter to consider only sites within ``sys``"""
        def shape(pos): return ta.array(pos, float) in id_by_pos.keys()
        return filter(shape, neigbrs)

    for k, tail in enumerate(sites):
        origin = tail.pos
        neigbrs = ref_site.family.n_closest(tail.pos, num_heads)
        neigbrs = valid_closest(neigbrs)
        for l, head_pos in enumerate(neigbrs):
            b = id_by_pos[ta.array(head_pos)]
            rij_matrix[k, l, :dim] = head_pos - origin
            indices[k, l] = b
    return indices, rij_matrix


def is_vector(vector):
    assert not np.all(vector == np.zeros(3))
    return isinstance(vector, (ta.ndarray_float, ta.ndarray_int, np.ndarray))


def correct_aniso(data):
    if is_vector(data):
        return [data]
    else:
        if isinstance(data, list):
            return data
        else:
            raise TypeError("aniso should be a vector or a list of vectors")


def _inspect(some_input, expected):
    """Checks whether the inputs correspond to the expected ones."""
    for var, _ in some_input.items():
        try:
            expected[var]
        except KeyError:
            raise NameError(str(var) + " " + "is not a valid field_input key. Only the following inputs are accepted:"
                            + " " + "dmi, aniso, demag, K and B2")
