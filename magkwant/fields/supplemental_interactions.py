import numpy as np
import tinyarray as ta
from collections import defaultdict

__all__ = ["Triangular_interactions"]


class Triangular_interactions:
    """Precomputes additional effective fields relevent for frustrated systems
    on Triangular or Square lattices.
    Only the information concerning the sites connections are computed.
    This assumes stationnary interactions, homogenous throughout sys.
    The fields are computed only if their strengths are non zero.

    Parameters
    ----------
    sys : `kwant.Builder`
    field_constants: dict
        Field parameters as keys, and their values are values.
        Contains:
        demag: float, default (None)
            Demagnetization strength (magnetic dipole strength).
        aniso: callable or 1darray, default (None)
            Anisotropy vector :math: `v_{aniso}`, it can be a vector or
            a site dependent function.
        dmi: callable or float
            Dzyaloshinskii-Moriya Interaction strength.
            If `dmi` is given as a constant, then the default DMI vector
            :math: `D (z \times r_{ij})` is used.
            If a function is provided, it should take two sites as parameters
            and return the corresponding DMI vector.
        K: four spin interaction
        Y: three spin interaction
        B2: biquadratic interaction
        B3: bicubic interaction
    true_triangular: bool, default (True)
        If set to `False` a square lattice is assumed.
        This doesn't lead to any difference for the four spin interaction.
        However, the bi-quadratic interaction would be different.
    pbc: bool, default (False)
        Whether periodic boundary conditions are considered or not.

    Note
    ----
        If pbc is activated then a periodic sys has to be given, otherwise, only the four spin and
        biquadratic interactions are computed with periodic boundary conditions.

    Attributes
    ----------
    h_4spin:
        Four spin interaction matrix.
        For a given site c depicted below,
               3
        4 x----x----x 2
          |    |    |
        5 x----c----x 1
          |    |    |
        6 x----x----x 8
               7
        the underlying 8 sites are stocked in `h_4spin`.

    h_bq:
        Biquadratic interaction matrix.
        for a given site all first nn sites are stocked.
    """

    def __init__(self, sys, field_constants, true_triangular=True, pbc=False):
        self.field_constants = field_constants
        self.K = self.field_constants["K"]
        self.Y = self.field_constants["Y"]
        self.B2 = self.field_constants["B2"]
        self.B3 = self.field_constants["B3"]
        self.true_triangular = true_triangular
        self.sys = sys
        self.pbc = pbc

        _sites = self.sys.sites()
        labeled_tags = defaultdict(int)

        for i, tail in enumerate(_sites):
            labeled_tags[tail.tag] = i
        self.num_sites = len(labeled_tags)  # number of sites

        self._min = min(labeled_tags)
        self._max = max(labeled_tags)
        self.period = self._max - self._min + 1

        self.labeled_tags = labeled_tags
        self.h_4spin = None
        self.h_bq = None

    def _jump_back(self, to, frm):
        move = to - frm
        new_head = np.array(to - np.sign(move) * self.period)
        for di in range(move.shape[0]):
            if self._min[di] <= to[di] <= self._max[di]:
                # if one direction of the end site reside within the
                # the underlying system's dimension then it's kept intact
                new_head[di] = to[di]
        return ta.array(new_head)

    def four_spin(self):
        if self.K is not None:
            labeled_tags = self.labeled_tags
            _four_spin = defaultdict(list)
            for ij, k in labeled_tags.items():
                i, j = ij
                full = [(i+1, j), (i+1, j+1), (i, j+1), (i-1, j+1),
                        (i-1, j), (i-1, j-1), (i, j-1), (i+1, j-1)]
                full = map(ta.array, full)
                for elem in full:
                    if elem in labeled_tags:
                        _four_spin[k].append(labeled_tags[elem])
                    else:
                        if self.pbc:
                            elem = self._jump_back(elem, ij)
                            _four_spin[k].append(labeled_tags[elem])

            self.h_4spin = np.zeros((self.num_sites, 8), dtype=np.int32)
            self.h_4spin -= 11  # used to mark the missing sites if pbc is False
            for i, li in _four_spin.items():
                for (index, j) in enumerate(li):
                    self.h_4spin[i, index] = j
        return self.h_4spin

    def biquadratic(self):
        if any(const is not None for const in [self.B2, self.B3, self.Y]):
            labeled_tags = self.labeled_tags
            bi_quadratic = defaultdict(list)
            for ij, k in labeled_tags.items():
                i, j = ij
                full = [(i+1, j), (i, j+1), (i-1, j), (i, j-1)]
                if self.true_triangular:
                    full += [(i-1, j+1), (i+1, j-1)]
                full = map(ta.array, full)
                for elem in full:
                    if elem in labeled_tags:
                        bi_quadratic[k].append(labeled_tags[elem])
                    else:
                        if self.pbc:
                            elem = self._jump_back(elem, ij)
                            bi_quadratic[k].append(labeled_tags[elem])

            if self.true_triangular:
                parteners_range = 6  # only first nearest neighbors
            else:
                parteners_range = 4
            self.h_bq = np.zeros(
                (self.num_sites, parteners_range), dtype=np.int32)
            self.h_bq -= 11
            # -11 represents a missing neighbor so its contribution is multiplied by zero
            # in the field calculator in supplemental fields
            for i, li in bi_quadratic.items():
                for (index, j) in enumerate(li):
                    self.h_bq[i, index] = j
        return self.h_bq
