cimport cython
import numpy as np

@cython.boundscheck(False)
@cython.wraparound(False)
def get_fields(double[:, :] M,  int[:, :] neighbors, double[:, :] h_exch,
               double[:, :] h_aniso=None, double[:, :, :] h_dmi=None, 
               double[:, :, :] h_dipole=None, int[:, :] indices=None):
    '''Computes the total field resulting from
       exchange, anisotropy, DMI and magneric dipole interactions.

    Parameters
    ---------
    M: ndarray
        Array of magnetization vectors
    neighbors: ndarray
        Array of neighbors
    h_exch: ndarray
        Array of hopoing constants corresponding to neighbors
    h_aniso: ndarray, default (None)
        Array of anisotropy field
    h_dmi: ndarray, default (None)
        Array of the dmi interaction vectors
    h_dipole: ndarray, default (None)
        Array of magnetic dipole vectors
    '''

    cdef Py_ssize_t num_sites = neighbors.shape[0]
    cdef Py_ssize_t num_neighbors = neighbors.shape[1]
    cdef Py_ssize_t num_dipoles = h_dipole.shape[1]
    cdef Py_ssize_t i, j, p, l
    cdef double h_x, h_y, h_z

    output = np.zeros((num_sites, 3), dtype=np.double)
    cdef double[:, :] output_view = output

    cdef int partner
    cdef double hop

    for i in range(num_sites):
        h_x = 0
        h_y = 0
        h_z = 0
        ###### anisotropy field ######
        #         Ki * Mi            #
        ##############################
        if h_aniso is not None:
            h_x += M[i, 0]  * h_aniso[i, 0]
            h_y += M[i, 1]  * h_aniso[i, 1]
            h_z += M[i, 2]  * h_aniso[i, 2]
        ###### exchange field ######
        #     \sum_j Jij * Mj      #
        ############################
        for p in range(num_neighbors):
            partner = neighbors[i, p]
            hop = h_exch[i, p]
            h_x += hop * M[partner, 0]
            h_y += hop * M[partner, 1]
            h_z += hop * M[partner, 2]

            ####### DMI field #######
            #    \sum_j Mj x Dij    #
            #########################
            if h_dmi is not None:
                h_x += h_dmi[i, p, 1] * M[i, 2] - h_dmi[i, p, 2] * M[i, 1]
                h_y += h_dmi[i, p, 2] * M[i, 0] - h_dmi[i, p, 0] * M[i, 2]
                h_z += h_dmi[i, p, 0] * M[i, 1] - h_dmi[i, p, 1] * M[i, 0]

        ###### demagnetization field ######
        #      \sum_j hdij * Mj           #
        ###################################
        if h_dipole is not None:
            if indices is not None:
                # dipole-dipole interaction with cutoff
                for j in range(num_dipoles):
                    l = indices[i, j]
                    if l != -11:
                        h_x += h_dipole[i, l, 0] * M[l, 0]
                        h_y += h_dipole[i, l, 1] * M[l, 1]
                        h_z += h_dipole[i, l, 2] * M[l, 2]
            else:
                # all to all dipole-dipole interaction
                for j in range(num_dipoles):
                    h_x += h_dipole[i, j, 0] * M[j, 0]
                    h_y += h_dipole[i, j, 1] * M[j, 1]
                    h_z += h_dipole[i, j, 2] * M[j, 2]

        output_view[i, 0] = h_x
        output_view[i, 1] = h_y
        output_view[i, 2] = h_z
    return output
