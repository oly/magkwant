"""Computes higher order Heisenberg interactions
beyond the bilinear term.
"""
cimport cython
import numpy as np

@cython.boundscheck(False)
@cython.wraparound(False)
def get_4spin(double[:, :] M,  int[:, :] h_4spin, double K=0):
    """Computes the four spin interaction field.
    """
    cdef Py_ssize_t i
    cdef Py_ssize_t j
    cdef Py_ssize_t num_sites = h_4spin.shape[0]
    cdef double[:] s_0 = np.zeros(3, dtype=np.double)
    cdef double p_j
    cdef double p_k
    cdef double p_l
    cdef double[:] s_j
    cdef double[:] s_k
    cdef double[:] s_l
    cdef int[:, :] sites = np.array([[0, 1, 2], [2, 3, 4], [4, 5, 6], [6, 7, 0]], dtype=np.int32)
    output = np.zeros((num_sites, 3), dtype=np.double)
    cdef double[:, :] output_view = output

    ##################### Four spin interaction ########################
    # :math: `\sum_{jkl}\{(M_k \dot M_l) M_j + (M_k \dot M_j) M_l       
    #                                        - (M_l \dot M_j) M_k\}`    
    # the summation is over all triplets surounding the concerned site. 
    # In the case of no pbc some triplets have to be omitted.           
    # The underlying sites are marked by the value -11.                 
    # The labeling of the sites interacting with the central site c, at 
    # which the four spin interaction is evaluated is as follows        
    #           2                                                       
    #    3 x----x----x 1                                                
    #      |    |    |                                                  
    #    4 x----c----x 0                                                
    #      |    |    |                                                  
    #    5 x----x----x 7                                                
    #           6                                                       
    # See also:                                                    
    # [Journal of Magnetism and Magnetic Materials 217 (2000) 216-224]  
    #####################################################################

    for i in range(num_sites):
        h_x = 0
        h_y = 0
        h_z = 0
        for j in range(4):
            if h_4spin[i, sites[j, 0]] != -11:
                s_j = M[h_4spin[i, sites[j, 0]], :]
            else:
                s_j = s_0
            if h_4spin[i, sites[j, 1]] != -11:
                s_k = M[h_4spin[i, sites[j, 1]], :]
            else:
                s_k = s_0
            if h_4spin[i, sites[j, 2]] != -11:
                s_l = M[h_4spin[i, sites[j, 2]], :]
            else:
                s_l = s_0

            p_j = s_k[0] * s_l[0] + s_k[1] * s_l[1] + s_k[2] * s_l[2]
            p_l = s_k[0] * s_j[0] + s_k[1] * s_j[1] + s_k[2] * s_j[2]
            p_k = s_j[0] * s_l[0] + s_j[1] * s_l[1] + s_j[2] * s_l[2]

            h_x += p_j * s_j[0] + p_l * s_l[0] - p_k * s_k[0]
            h_y += p_j * s_j[1] + p_l * s_l[1] - p_k * s_k[1] 
            h_z += p_j * s_j[2] + p_l * s_l[2] - p_k * s_k[2]
        
        output_view[i, 0] = K * h_x 
        output_view[i, 1] = K * h_y
        output_view[i, 2] = K * h_z
         
    return output


@cython.boundscheck(False)
@cython.wraparound(False)
def get_others(double[:, :] M,  int[:, :] h_bq, double B2=0, double B3=0, double Y=0):
    """Computes the biquadratic, bicubic and the three spin fileds.

    See also
    --------
    [PRB 101, 024418 (2020)].
    """
    cdef Py_ssize_t num_sites = h_bq.shape[0]
    cdef Py_ssize_t num_neighbors = h_bq.shape[1]
    cdef Py_ssize_t i
    cdef Py_ssize_t j
    cdef int p
    cdef int q
    cdef double h_x
    cdef double h_y
    cdef double h_z
    cdef double scalar
    cdef double scalar_iq
    cdef double scalar_pq
    cdef double prefactor
    output = np.zeros((num_sites, 3), dtype=np.double)
    cdef double[:, :] output_view = output

    for i in range(num_sites):
        h_x = 0.0
        h_y = 0.0
        h_z = 0.0
        for j in range(num_neighbors):
            p = h_bq[i, j]

            if p != -11:
                ########### Biquadratic field ###############
                # :math: `2 B_2 \sum_{p} M_p (M_i \dot M_p)`#
                #############################################
                scalar = (M[i, 0] * M[p, 0] + M[i, 1] * M[p, 1] + M[i, 2] * M[p, 2])
                prefactor = 0.0
                if B2 != 0:
                    prefactor += 2 * B2 * scalar

                if B3 != 0:
                    ################ Bicubic field #################
                    # :math: `3 B_3 \sum_{p} M_p (M_i \dot M_p)^2` #
                    ################################################
                    prefactor += 3 * B3 * scalar * scalar

                h_x += prefactor * M[p, 0]
                h_y += prefactor * M[p, 1]
                h_z += prefactor * M[p, 2]

                if Y != 0:
                    ######################### Three spin field #############################
                    # :math: `Y \sum_{pq} M_p (M_i \dot M_q) + (M_p + M_q) (M_p \dot M_q)` #
                    ########################################################################
                    q = h_bq[i, j-1]
                    # i, p and q form the three spin triangle
                    if q != -11:
                        scalar_iq = (M[i, 0] * M[q, 0] + M[i, 1] * M[q, 1] + M[i, 2] * M[q, 2])
                        scalar_pq = (M[p, 0] * M[q, 0] + M[p, 1] * M[q, 1] + M[p, 2] * M[q, 2])
                        h_x += Y * (M[p, 0] * scalar_iq + (M[p, 0] + M[q, 0]) * scalar_pq)
                        h_y += Y * (M[p, 1] * scalar_iq + (M[p, 1] + M[q, 1]) * scalar_pq)
                        h_z += Y * (M[p, 2] * scalar_iq + (M[p, 2] + M[q, 2]) * scalar_pq)

        output_view[i, 0] = h_x
        output_view[i, 1] = h_y
        output_view[i, 2] = h_z

    return output
